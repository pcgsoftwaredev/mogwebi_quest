﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour
{
    private Animator anim;

    public float delayCounter = 0;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        Sound_Manager.Instance.PlaySound("coin_sound");
        StartCoroutine("Destroy");
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(3);
        Destroy(gameObject.transform.parent.gameObject);
    }
}