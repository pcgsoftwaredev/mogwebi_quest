﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DanielLochner.Assets.SimpleScrollSnap;
using UnityEngine.UI;
using System.IO;
using System;

public class GlossaryChallenge : MonoBehaviour
{
    /// <summary>
    /// HARDCODED DEMO VERSION FOR GLOSSARY CHALLENGE
    /// </summary>
    public GameObject infoPanel;
    public TMP_Text challengeTitle, y1_text, y2_text,y3_text, x1_text, x2_text, x3_text, challengeCount, Timer, Score, Reward, QuestionsRemaining;
    public bool isSpinning, runTimer, infoShowing;
    public TMP_Text[] answers;
    SimpleScrollSnap sss;
    GlossaryItem[] glossaryItems;
    MiniGamePlayer player;
    private string dataFile = "glossary_questionaire.json";
    private static string Answer1, Answer2, Answer3;

    public static int numOccupiedSlots = 0;
    public static int numCorrectSlots = 0;

    // Start is called before the first frame update
    void Start()
    {
        
        sss = GetComponentInChildren<SimpleScrollSnap>();
        LoadData();
        prepareBoard();

        Initialise();
    }

    // Update is called once per frame
    void Update()
    {
       UpdateTimer();
       UpdateScore();
    }

     void Initialise()
    {
		isSpinning   = false;
        runTimer     = true;
        infoShowing  = false;

        player = new MiniGamePlayer();
        player.totalreward = 150;
    }

    void UpdateScore()
    {
        player.score = numCorrectSlots;
        Score.text  = player.score + "/" + player.totalchances;
        Reward.text = player.reward+""; 
    }


    public static void SetSlotAnswer(string slot, GameObject obj)
    {
        switch (slot)
        {
            case "slot1":
                ValidateAnswer(Answer1, obj);
                break;
            case "slot2":
                ValidateAnswer(Answer2, obj);
                break;
            case "slot3":
                ValidateAnswer(Answer3, obj);
                break;
        }
    }

    private static void ValidateAnswer(string correctAnswer, GameObject obj)
    {
        Color wrong = Color.red;
        Color correct = Color.green;
        string answer = obj.GetComponentInChildren<TextMeshProUGUI>().text;
        
        if (answer.ToLower().Equals(correctAnswer.ToLower()))
        {
            //do sounds and animation for correct and increase FinIQ or whatever
            obj.transform.GetChild(0).GetComponent<Image>().color = correct;
            numCorrectSlots ++;
        }
        else
        {
            //do sounds and animation for wrong
            obj.transform.GetChild(0).GetComponent<Image>().color = wrong;

        }
    }

    public void LoadData()
    {
        string dataFilePath = Path.Combine(Application.persistentDataPath, dataFile);
        
        //TODO: Handle Error.
        if(File.Exists(dataFilePath))
        {
            string dataAsJson = File.ReadAllText(dataFilePath);
            glossaryItems = JsonHelper.FromJson<GlossaryItem>(dataAsJson);

        }else{
            
            dataFilePath = Path.Combine(Application.streamingAssetsPath,dataFile);
           
            //TODO: Handle Error.
            if(File.Exists(dataFilePath))
            {
                string dataAsJson = File.ReadAllText(dataFilePath);
                glossaryItems = JsonHelper.FromJson<GlossaryItem>(dataAsJson);
            }
        }
        
        print("data file path :"+dataFilePath);
    }

    public void ToggleInfoPopup()
    {
        if (infoPanel.activeSelf)
        {
            infoPanel.SetActive(false);
        }
        else
        {
            infoPanel.SetActive(true);
        }
    }

    public void prepareBoard()
    {
        GlossaryItem[] itemsList = shuffleAnswers();
        System.Random rand = new System.Random();

        Answer1 = itemsList[0].term;
        Answer2 = itemsList[1].term;
        Answer3 = itemsList[2].term;

        //TODO: Read List instead of Array from json file. This is a temporary fix.
        List<int> indexes = new List<int>();

        for(int k=0; k<3; k++)
        {
            indexes.Add(k);
        }


        int i = 0, index;
        while(indexes.Count >0)
        {
            index = rand.Next(indexes.Count);
            answers[i].text = itemsList[indexes[index]].term;
            indexes.RemoveAt(index);
            i++;
        }

        x1_text.text = itemsList[0].definition;
        x2_text.text = itemsList[1].definition;
        x3_text.text = itemsList[2].definition;

    }

    public void LoadNextChallenge()
    {
       
    }

    void UpdateTimer()
    {
        if(!player.gameOver && runTimer)
        {
            player.time -= Time.deltaTime;
            if (player.time >= 0)
            {
                int seconds = (int)player.time % 60;
                int minutes = (int)player.time / 60;
                string time = minutes + ":" + seconds;
                if (seconds < 10)
                    time = minutes + ":0" + seconds;

                Timer.text = time;
            }
            else
            {
                player.gameOver = true;
            }
        }
        
    }

    public GlossaryItem[] shuffleAnswers()
   {
      GlossaryItem[] shuffledList = new GlossaryItem[glossaryItems.Length];
      GlossaryItem[] originalList = this.glossaryItems;
      System.Random rand = new System.Random();

      //TODO: Read List instead of Array from json file. This is a temporary fix.
      List<int> indexes = new List<int>();

      for(int k=0; k<this.glossaryItems.Length; k++)
      {
        indexes.Add(k);
      }

      int i = 0, index;
      while(indexes.Count >0)
      {
         index = rand.Next(indexes.Count);
         shuffledList[i] = originalList[indexes[index]];
         indexes.RemoveAt(index);
         i++;
      }

      return shuffledList;
   }


   public void setPlayerScore()
   {
       player.score = numCorrectSlots;
   }
}

[Serializable]
public class GlossaryItem
{
    public string term;
    public string definition;
}