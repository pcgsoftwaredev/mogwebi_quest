﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerButtonBehavior : MonoBehaviour
{
    public Answer answer;
    public GameObject parentObject;
    private Image imageComponent;
    
    // Start is called before the first frame update
    void Start()
    {
        this.imageComponent = this.gameObject.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setAnswer(Answer answer)
    {
        this.answer = answer;

        if(this.gameObject.GetComponent<Image>() !=null)
        {
            Color32 white = new Color32(255, 255, 255,255);
            this.gameObject.GetComponent<Image>().color = white;
        }else{
            print("NO IMAGECOMPONENT");
        }
        
    }

    public void checkAnswer()
    {

        print("Answer clicked.."+this.answer.correct);
        if(this.answer.correct)
        {
            Color32 green = new Color32(53, 203, 59,255);
            this.imageComponent.color = green;
        }else{
            Color32 red = new Color32(248, 123, 123, 255);
            this.imageComponent.color = red;
        }

        this.parentObject.GetComponent<PlayScript>().CheckAnswer(this.answer.correct);
    }

    public void showCorrectAnswer()
    {
        if(this.answer.correct)
        {
            FlashCorrectAnswer();
        }
    }

    void FlashCorrectAnswer()
    {
        StopCoroutine("Flash");
        StartCoroutine("Flash");
    }

    IEnumerator Flash()
    {
        int count = 6;
        this.imageComponent.color = new Color32(53, 203, 59, 255);
        while (count > 0)
        {
            count --;
            switch (this.imageComponent.color.a.ToString())
            {
                case "0":
                    this.imageComponent.color = new Color(this.imageComponent.color.r, this.imageComponent.color.g, this.imageComponent.color.b, 1);
                    yield return WaitForUnscaledSeconds(0.5f);
                    break;

                case "1":
                    this.imageComponent.color = new Color(this.imageComponent.color.r,this.imageComponent.color.g, this.imageComponent.color.b, 0);
                    yield return WaitForUnscaledSeconds(0.5f);
                    break;
            }
        }
        this.parentObject.GetComponent<PlayScript>().nextQuestion();
    }

    IEnumerator WaitForUnscaledSeconds(float dur)
    {
        var cur = 0f;
        while (cur < dur)
        {
            yield return null;
            cur += Time.unscaledDeltaTime;
        }
    }
}
