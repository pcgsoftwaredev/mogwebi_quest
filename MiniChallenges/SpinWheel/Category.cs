﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class Category
{
    public string name;

    public string type;

    public Question[] questions;


    public Question getQuestion(){
        
        Random rnd = new Random();
        int questionIndex = rnd.Next(questions.Length);

        return questions[questionIndex];
    }
}