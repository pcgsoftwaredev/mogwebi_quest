﻿
public class MiniGamePlayer
{

    public int reward{
        get {
            return score * (totalreward/totalchances); 
        }
    }
    public int totalreward{
        get; set;
    }

    public int totalchances
    {
        get;set;
    }
    public int score
    {
        get; set;
    }
    public float time
    {
        get; set;
    }
    public bool gameOver
    {
        get; set;
    }

    public int chances
    {
        get; set;
    }

    public MiniGamePlayer()
    {
        this.time      = 120;
        this.score     = 0;
        this.gameOver  = false;
        this.chances   = 2;
        this.totalchances = 2;
    }

    public void addScore()
    {
        this.score ++;
    }

    public void deductChances()
    {
        if(chances > 0)
        {
            this.chances --;
        }
        else{
            this.gameOver = true;
        }
    }

    public bool hasChances()
    {
        if(this.chances > 0)
        {
            return true;
        }else{
            this.gameOver = true;
            return false;
        }
    }


}
