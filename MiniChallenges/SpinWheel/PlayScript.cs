﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayScript : MonoBehaviour
{

    private string categoriesFile = "categories_data.json";
    Category[] categories;
    
    public GameObject questionText;
    public GameObject[] answers;
    public GameObject[] options;
    public GameObject wheel,pointer,btn_spin,challenge_complete;
    public GameObject questionPanel, infoPanel;
    public TMP_Text Timer, Score, Reward, QuestionsRemaining;
    private int randomTime;
    private int itemNumber;

    private float anglePerItem;	
    public bool isSpinning, runTimer, infoShowing;
    private int _reward;
    private int _score;
    private float _time;
    private bool _gameOver;

    Question selectedQuestion;
    MiniGamePlayer player;

    // Start is called before the first frame update
    void Start()
    {
        LoadCategories();
        PrepareWheel();
    
        Initialise();
    }

    void Initialise()
    {
        _time      = 120;
        _score     = 0;
        _gameOver  = false;

        anglePerItem = 360/4;	
		isSpinning   = false;
        runTimer     = false;
        infoShowing  = false;

        player = new MiniGamePlayer();
        player.totalreward = 10;
        player.time = _time;
        player.score = _score;
        player.gameOver = _gameOver;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateScore();
        UpdateTimer();
        QuestionsRemaining.text = ""+player.chances;
    }

    //Read and Load Categories into the categories Array,
    //Needed to prepare the Wheel.
    public void LoadCategories()
    {
        string dataFilePath = Path.Combine(Application.persistentDataPath, categoriesFile);
        
        //TODO: Handle Error.
        if(File.Exists(dataFilePath))
        {
            string dataAsJson = File.ReadAllText(dataFilePath);
            categories = JsonHelper.FromJson<Category>(dataAsJson);

        }else{
            
            dataFilePath = Path.Combine(Application.streamingAssetsPath,categoriesFile);
           
            //TODO: Handle Error.
            if(File.Exists(dataFilePath))
            {
                string dataAsJson = File.ReadAllText(dataFilePath);
                categories = JsonHelper.FromJson<Category>(dataAsJson);
            }
        }
        
        print("data file path :"+dataFilePath);
    }

    void UpdateScore()
    {
        Score.text  = player.score + "/" + player.totalchances;
        Reward.text = player.reward+""; 
    }

    void UpdateTimer()
    {
        if(!player.gameOver && runTimer)
        {
            player.time -= Time.unscaledDeltaTime;
            if (player.time >= 0)
            {
                int seconds = (int)player.time % 60;
                int minutes = (int)player.time / 60;
                string time = minutes + ":" + seconds;
                if (seconds < 10)
                    time = minutes + ":0" + seconds;

                Timer.text = time;
            }
            else
            {
                player.gameOver = true;
            }
        }
        
    }


    public void PrepareWheel()
    {
        wheel.GetComponent<SpinWheel>().setCategories(categories);
    }

    public void showQuestion(Question question)
    {
        wheel.SetActive(false);
        btn_spin.SetActive(false);
        pointer.SetActive(false);
        questionPanel.SetActive(true);
        
        questionText.GetComponent<TextMeshProUGUI>().text = question.value;

        //Get Shuffled Answers
        Answer[] shuffledAnswers = question.shuffleAnswers();

        for(int i = 0; i < question.answers.Length; i++)
        {
            answers[i].GetComponent<TextMeshProUGUI>().text = shuffledAnswers[i].value;
            options[i].GetComponent<AnswerButtonBehavior>().setAnswer(shuffledAnswers[i]);
        }

        player.deductChances();
        runTimer = true;
    }


    public Question getQuestion(Category category)
    {
        Question question = null;
        if(category.questions.Length > 0)
        {
            question = category.questions[Random.Range (0, category.questions.Length)];
        }

        return question;

    }


    public void displayQuestion(Category category)
    {
        showQuestion(getQuestion(category));
    }


    public void SpinWheel()
    {
        if(!infoShowing)
        {
            wheel.GetComponent<SpinWheel>().setCategories(categories);
            wheel.GetComponent<SpinWheel>().Spin();
        }
        
    }


    public void CheckAnswer(bool answer)
    {
        if(!answer)
        {
            foreach (GameObject item in options)
            {
                item.GetComponent<AnswerButtonBehavior>().showCorrectAnswer();
            }
        }else{
            player.addScore();
            StartCoroutine("answerDelay");
        }

        if (!player.hasChances())
            endGame();
    }


    public void nextQuestion()
    {
        if(player.hasChances())
        {
            questionPanel.SetActive(false);
            wheel.SetActive(true);
            btn_spin.SetActive(true);
            pointer.SetActive(true);
            runTimer = false;
        }
    }

    IEnumerator  answerDelay()
    {
        yield return WaitForUnscaledSeconds(1.5f);          
        nextQuestion();
    }

    public void endGame()
    {
        if(player.gameOver)
        {
            wheel.SetActive(false);
            questionPanel.SetActive(false);
            btn_spin.SetActive(false);
            pointer.SetActive(false);
            challenge_complete.SetActive(true);
            GameManager.CurrentFinIQ += player.reward;
            Level_1.Instance.Exit();
            Destroy(gameObject);
        }
    }

    public void showInfo()
    {
        if(!infoShowing)
        {
            infoPanel.SetActive(true);
            infoShowing = true;
        }
        else
        {
            infoPanel.SetActive(false);
            infoShowing = false;
        }
    }

    public void setScore()
    {
        
    }

    public void QuitWOF()
	{
        GameManager.Instance.UnPauseGame();
		Destroy(gameObject);
	}

    IEnumerator WaitForUnscaledSeconds(float dur)
    {
        var cur = 0f;
        while (cur < dur)
        {
            yield return null;
            cur += Time.unscaledDeltaTime;
        }
    }
}
