﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class Question
{
   public string value;

   public Answer[] answers;

   public Answer[] shuffleAnswers()
   {
      Answer[] shuffledList = new Answer[4];
      Answer[] originalList = this.answers;
      Random rand = new Random();

      //TODO: Read List instead of Array from json file. This is a temporary fix.
      List<int> indexes = new List<int>();
      indexes.Add(0);
      indexes.Add(1);
      indexes.Add(2);
      indexes.Add(3);

      int i = 0, index;
      while(indexes.Count >0)
      {
         index = rand.Next(indexes.Count);
         shuffledList[i] = originalList[indexes[index]];
         indexes.RemoveAt(index);
         i++;
      }

      return shuffledList;
   }
}
