using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;

public class SpinWheel : MonoBehaviour
{
	public List<AnimationCurve> animationCurves;
	private string categoriesFile = "categories_data.json";	
	Category[] categories;	
	private float anglePerItem;	
	private int randomTime;
	private int itemNumber;
	private Question selected_question;
    public GameObject[] answers;
	public GameObject[] categoriesTxt;

	public GameObject parentObject;

	public bool isSpinning;
	
	void Start()
	{
		anglePerItem = 360/4;
	}
	
	void  Update ()
	{
	
	}
	

	IEnumerator SpinTheWheel (float time, float maxAngle)
	{
		float timer = 0.0f;		
		float startAngle = transform.eulerAngles.z;		
		maxAngle = maxAngle - startAngle;
		
		int animationCurveNumber = Random.Range (0, animationCurves.Count);
		
		while (timer < time) 
		{
			float angle = maxAngle * animationCurves [animationCurveNumber].Evaluate (timer / time) ;
			transform.eulerAngles = new Vector3 (0.0f, 0.0f, angle + startAngle);
			timer += Time.unscaledDeltaTime;
			yield return null;
            print("spinning");

		}
	
		transform.eulerAngles = new Vector3 (0.0f, 0.0f, maxAngle + startAngle);

		yield return WaitForUnscaledSeconds(1f);
	
		print("stopped spinning..");

		//this.GetComponent<Animator>().SetBool("spinfinished",true);
        this.showQuestion();
	}	

	public void showQuestion()
	{
		parentObject.GetComponent<PlayScript>().displayQuestion(categories[itemNumber]);
	}

	public void Spin()
	{
			randomTime     = 4;
			itemNumber     = Random.Range (0,4);
			float maxAngle = 360 * randomTime + (itemNumber * anglePerItem);

			StartCoroutine (SpinTheWheel (randomTime, maxAngle));
		
	}


	public void setCategories(Category[] categories)
	{
		this.categories = categories;

		for(int i = 0; i < this.categories.Length; i++)
		{
			categoriesTxt[i].GetComponent<TextMeshProUGUI>().text = categories[i].name;
		}
	}

    IEnumerator WaitForUnscaledSeconds(float dur)
    {
        var cur = 0f;
        while (cur < dur)
        {
            yield return null;
            cur += Time.unscaledDeltaTime;
        }
    }

}