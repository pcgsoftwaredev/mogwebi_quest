﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CookingDough : MonoBehaviour
{
    private Animator anim;
    [SerializeField]private GameObject servingTray;

    [SerializeField]private GameObject cookingDoughParticles;
    [SerializeField]private GameObject cookedFatCakesParticles;
    [SerializeField]private GameObject cookingDoughSmoke;
    private ParticleSystem.MainModule settings;

    public enum CookingDoughState
    {
        cooking,
        cooked,
        burnt,
        moveToBin,
        onServingTray
    }

    public int currentGrillSlot = 0;

    private CookingDoughState cookingDoughState;

    [SerializeField]private Sprite cookedFatCakesSprite;

    private float ellapseTime;
    private bool isCountingDown = false;

    private int servingsCount = 0;
    int DeepFrySfx;

    // Start is called before the first frame update
    void Start()
    {

        //anim = GetComponent<Animator>();
        cookingDoughState = CookingDoughState.cooking;

        DeepFrySfx = Sound_Manager.Instance.PlaySound("deep_frying");

        settings = cookedFatCakesParticles.GetComponent<ParticleSystem>().main;

        StartTimer();
    }

    // Update is called once per frame
    void Update()
    {
        _tick();

        if (servingsCount >= 2)
            Object.Destroy(this.gameObject);

        if (isCountingDown)
        {
            if (ellapseTime > 5 && ellapseTime <= 10)
            { // Hotdog is cooked when it has been cooking for more than 5 seconds
                cookingDoughState = CookingDoughState.cooked;
            }
            else if (ellapseTime > 10 && ellapseTime <= 13)
            { // Hotdog is burnt after 10 seconds
                cookingDoughState = CookingDoughState.burnt;
            }
            else if (ellapseTime > 13)
            { // Hotdog is automatically moved to the bin after 3 seconds of being burnt
                cookingDoughState = CookingDoughState.moveToBin;
                isCountingDown = false;
            }
        }

        switch (cookingDoughState)
        {
            case CookingDoughState.cooking:
                // Run cooking animation
                break;
            case CookingDoughState.cooked:
                // Change color to a bit darker color to show that its cooked
                if(!cookedFatCakesParticles.activeSelf)
                    cookedFatCakesParticles.SetActive(true);
                if(cookingDoughParticles.activeSelf)
                    cookingDoughParticles.SetActive(false);

                //anim.SetBool("isCooked", true);
                break;
            case CookingDoughState.burnt:
                // Change color to burnt color
                settings.startColor = new Color(0.2f, 0.2f, 0.2f, 1.0f);

                // Activate the smoke
                if (cookingDoughSmoke)
                    cookingDoughSmoke.gameObject.SetActive(true);

                break;
            case CookingDoughState.moveToBin:
                // Destroy instance and make the bin shake, clear the slot for other doughs to cook
                Object.Destroy(this.gameObject);

                // Play sound effect of dropping into the trash bin
                Sound_Manager.Instance.PlaySound("trash_item");
                break;
            case CookingDoughState.onServingTray:
                // You can now served with bun
                Object.Destroy(this.gameObject);
                break;
            default:
                Object.Destroy(this.gameObject);
                break;

        }
    }

    private void OnDestroy()
    {
        if (DoubleDeepFry.isInitialized)
        {
            DoubleDeepFry.DoughCooking = false;
        }

        Sound_Manager.Instance.StopSound(DeepFrySfx);
    }

    private void OnMouseDown()
    {
        if (cookingDoughState == CookingDoughState.cooked)
        {
            if (servingsCount <= 2)
            {
                // Serve cooked fries to the serving area
                servingsCount++;
                ServingContainer.Instance.PlaceFatCakesOrder();
            }
        }

        // Play the animation for feedbacl
        //anim.Play("hotDogsContainerAnim", 0);
    }

    // Simple timer
    public void StartTimer()
    {
        if (!isCountingDown)
        {
            isCountingDown = true;
            ellapseTime = 0;
        }
    }

    private void _tick()
    {
        if (isCountingDown)
        {
            ellapseTime += Time.deltaTime;
        }
    }
}
