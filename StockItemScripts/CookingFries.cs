﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CookingFries : MonoBehaviour
{
    private Animator anim;
    [SerializeField]private GameObject servingTray;
    [SerializeField]private GameObject cookingFriesSmoke;

    [SerializeField]private ParticleSystem friesParticles1;
    [SerializeField]private ParticleSystem friesParticles2;
    private ParticleSystem.MainModule settings1;
    private ParticleSystem.MainModule settings2;

    public enum CookingFriesState
    {
        cooking,
        cooked,
        burnt,
        moveToBin,
        onServingTray
    }

    public int currentGrillSlot = 0;

    public CookingFriesState cookingFriesState;


    private float ellapseTime;
    private bool isCountingDown = false;
    private int servingsCount = 0;
    private int DeepFrySfx;

    // Start is called before the first frame update
    void Start()
    {
        StartTimer();

        //anim = GetComponent<Animator>();
        cookingFriesState = CookingFriesState.cooking;

        DeepFrySfx = Sound_Manager.Instance.PlaySound("deep_frying");

        settings1 = friesParticles1.GetComponent<ParticleSystem>().main;
        settings2 = friesParticles2.GetComponent<ParticleSystem>().main;
    }

    // Update is called once per frame
    void Update()
    {
        _tick();

        if (servingsCount >= 2)
            Object.Destroy(this.gameObject);

        if (isCountingDown)
        {
            if (ellapseTime > 5 && ellapseTime <= 10)
            { // Hotdog is cooked when it has been cooking for more than 5 seconds
                cookingFriesState = CookingFriesState.cooked;
            }
            else if (ellapseTime > 10 && ellapseTime <= 13)
            { // Hotdog is burnt after 10 seconds
                cookingFriesState = CookingFriesState.burnt;
            }
            else if (ellapseTime > 13)
            { // Hotdog is automatically moved to the bin after 3 seconds of being burnt
                cookingFriesState = CookingFriesState.moveToBin;
                isCountingDown = false;
            }
        }

        switch (cookingFriesState)
        {
            case CookingFriesState.cooking:
                // Run cooking animation
                break;
            case CookingFriesState.cooked:
                // Change color to a bit darker color to show that its cooked
                settings1.startColor = new Color(0.7f, 0.6f, 0.6f, 1.0f);
                settings2.startColor = new Color(0.7f, 0.6f, 0.6f, 1.0f);

                // anim.SetBool("isCooked", true);
                break;
            case CookingFriesState.burnt:
                settings1.startColor = new Color(0.2f, 0.2f, 0.2f, 1.0f);
                settings2.startColor = new Color(0.2f, 0.2f, 0.2f, 1.0f);


                // Activate the smoke
                if (cookingFriesSmoke)
                    cookingFriesSmoke.gameObject.SetActive(true);
                break;
            case CookingFriesState.moveToBin:
                // Destroy instance and make the bin shake, clear the slot for other doughs to cook
                Object.Destroy(this.gameObject);

                // Play sound effect of dropping into the trash bin
                Sound_Manager.Instance.PlaySound("trash_item");
                break;
            case CookingFriesState.onServingTray:
                // You can now served with bun
                Object.Destroy(this.gameObject);
                break;
            default:

                Object.Destroy(this.gameObject);
                break;

        }
    }

    private void OnDestroy()
    {
        if (SingleDeepFryer.isInitialized)
        {
            SingleDeepFryer.FriesCooking = false;
        }
        else if (DoubleDeepFry.isInitialized)
        {
            DoubleDeepFry.FriesCooking = false;
        }

        Sound_Manager.Instance.StopSound(DeepFrySfx);
    }

    private void OnMouseDown()
    {
        if (cookingFriesState == CookingFriesState.cooked)
        {
            if (servingsCount <= 2)
            {
                // Serve cooked fries to the serving area
                servingsCount++;
                ServingContainer.Instance.PlaceFriesOrder();
            }
        }

        // Play the animation for feedbacl
        //anim.Play("hotDogsContainerAnim", 0);
    }

    // Simple timer
    public void StartTimer()
    {
        if (!isCountingDown)
        {
            isCountingDown = true;
            ellapseTime = 0;
        }
    }

    private void _tick()
    {
        if (isCountingDown)
        {
            ellapseTime += Time.deltaTime;
        }
    }
}
