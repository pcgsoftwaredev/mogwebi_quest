﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoughContainer : MonoBehaviour
{
    [SerializeField] private GameObject cookingDough;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        // Stark cooking mabanse on the second cooking area of the double deef fryer
        if (DoubleDeepFry.isInitialized)
        {
            if (!DoubleDeepFry.DoughCooking)
            {
                Instantiate(cookingDough, DoubleDeepFry.Instance.FatCakesSpot, Quaternion.identity);

                DoubleDeepFry.DoughCooking = true;
            }
        }
    }

    private void OnDestroy()
    {
        if(SoundManager.isInitialized)
             SoundManager.Instance.StopOilFryingSound();
    }
}
