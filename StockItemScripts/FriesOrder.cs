﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriesOrder : MonoBehaviour
{
    private bool dragging = false;
    private bool draggedRight = false;

    private Camera sceneCamera;
    private float distance;
    private Vector2 originalPosition;

    void Start()
    {
        sceneCamera = GameObject.FindGameObjectWithTag("SceneCamera").gameObject.GetComponent<Camera>();
        // orderState = OrderState.hasBun;

        // Play the bun drop sound


        // Store the original position incase the object is dragged where there is no customer, so you can return it
        originalPosition = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        // If the bun is being dragged
        if (dragging)
        {
            Ray ray = sceneCamera.ScreenPointToRay(Input.mousePosition);
            Vector3 rayPoint = ray.GetPoint(distance);
            transform.position = rayPoint;
        }
    }

    private void OnMouseDown()
    {
        distance = Vector3.Distance(transform.position, sceneCamera.transform.position);
        dragging = true;
    }

    void OnMouseUp()
    {
        dragging = false;
        // Debug.Log("But mouse went down");
        StartCoroutine(CheckForCorrectDrag());
    }

    IEnumerator CheckForCorrectDrag()
    {

        if (dragging == false)
        {
            if (!draggedRight)
            {
                transform.position = originalPosition;
            }
        }
        yield return null; // Wait for halp a second after drop and drag back
    }
}
