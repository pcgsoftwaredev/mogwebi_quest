﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HotDog : MonoBehaviour
{
    [SerializeField] private SpriteRenderer orderSprite;
    

    private Camera sceneCamera;
  
    public enum OrderState
    {
        Bun,
        PlainHotdog,
        HasKetchup,
        HasMustard,
        FullHouse,
    }

    [SerializeField]private Sprite hotDog_Plain;
    [SerializeField]private Sprite hotDog_Ketchup;
    [SerializeField]private Sprite hotDog_Mustard;
    [SerializeField]private Sprite hotDog_FullHouse;

    public OrderState HotDogOrderState { set;  get;}

    //drag-drop variables
    private bool draggable;
    private Vector3 screenPoint;
    private Vector3 position;
    private float dragOffset;
    private Rigidbody hotDogRB;
    private Vector3 positionOnTray;
    private Transform TrayTransform;
    private BoxCollider hotDogCollider;

    public bool IsSelected { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        sceneCamera = GameObject.FindGameObjectWithTag("SceneCamera").gameObject.GetComponent<Camera>();
        hotDogRB = gameObject.GetComponent<Rigidbody>();
        hotDogRB.isKinematic = true;
        hotDogCollider = gameObject.GetComponent<BoxCollider>();
        HotDogOrderState = OrderState.Bun;
        draggable = false;
        dragOffset = 20f;

        // Served in a hotdog container in level 3 and 2
        if (Level_1.isInitialized) // If this instance exists then you are in level
        {
            GameObject.FindGameObjectWithTag("HotDogContainer").SetActive(false);
        }
        else
        {
            GameObject.FindGameObjectWithTag("HotDogContainer").SetActive(true);
        }
        IsSelected = false;

       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetHotDogOrderSprite()
    {
        switch (HotDogOrderState)
        {
            case OrderState.PlainHotdog:
                orderSprite.sprite = hotDog_Plain;
                break;
            case OrderState.HasKetchup:
                orderSprite.sprite = hotDog_Ketchup;
                break;
            case OrderState.HasMustard:
                orderSprite.sprite = hotDog_Mustard;
                break;
            case OrderState.FullHouse:
                orderSprite.sprite = hotDog_FullHouse;
                break;
        }

        //the hotdog can now be served to customer
        draggable = true;
        hotDogCollider.size = new Vector3(1.4f, 0.6f, 1f);
    }

    private void OnCollisionEnter(Collision otherObj)
    {
        if (otherObj.transform.GetChild(1).CompareTag("BunDetector") && HotDogOrderState.Equals(OrderState.Bun))
        {
            //print("Sausage pinging me");
            gameObject.transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
        }
    }

    private void OnCollisionExit(Collision otherObj)
    {
        if (otherObj.transform.GetChild(1).CompareTag("BunDetector") && HotDogOrderState.Equals(OrderState.Bun))
        {
            //print("Sausage left");
            gameObject.transform.localScale = Vector3.one;
        }
    }

    private void OnMouseDown()
    {
        TrayTransform = gameObject.transform.parent;
        positionOnTray = gameObject.transform.position;
        screenPoint = sceneCamera.WorldToScreenPoint(positionOnTray);
        position = sceneCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y + dragOffset, screenPoint.z));
        if (draggable)
        {
            Tray.Instance.SelectTrayItem(gameObject);
            gameObject.transform.SetParent(transform.root);
            gameObject.transform.position = position;
            hotDogRB.isKinematic = false;
        }

        //EventSystem.current.SetSelectedGameObject(gameObject);
    }

    private void OnMouseUp()
    {
        if (draggable)
        {
            gameObject.transform.SetParent(TrayTransform);
            gameObject.transform.position = positionOnTray;
            hotDogRB.isKinematic = true;
        }
        
    }

    private void OnMouseDrag()
    {
        Vector3 newScreenPpoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);// current screen point
        position = sceneCamera.ScreenToWorldPoint(new Vector3(newScreenPpoint.x, newScreenPpoint.y + dragOffset, newScreenPpoint.z));
        hotDogRB.velocity = (position - gameObject.transform.position) * 50;
    }


}
