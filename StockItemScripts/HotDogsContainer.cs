﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HotDogsContainer : Singleton<HotDogsContainer>
{

    [SerializeField] private GameObject SausagePrefab;
    private GameObject sausageWidget;

    public static int HotDogCount { get; set; }

    private Animator anim;
    private BoxCollider2D tapArea;

    // Start is called before the first frame update
    void Start()
    {
        switch (GameManager.LevelData.levelNumber)
        {
            case 1:
                sausageWidget = Level_1.Instance.stockCntWidgets["sausage"];
                break;
        }
        // Set the initial count of hotdogs
        HotDogCount = int.Parse(sausageWidget.GetComponentInChildren<TextMeshProUGUI>().text);

        anim = GetComponent<Animator>();
        tapArea = GetComponent<BoxCollider2D>();

        GameManager.Instance.OnGameStateChanged.AddListener(DisableCollider);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void DisableCollider(GameManager.GameState currentState, GameManager.GameState prevState)
    {
        if (currentState == GameManager.GameState.PAUSED)
            tapArea.enabled = false;

        if (currentState == GameManager.GameState.LEVEL_RUNNING)
            tapArea.enabled = true;
    }

    private void OnMouseDown()
    {
        if (HotDogCount > 0 && GameManager.LevelPrepared)
        { // If there are hotdogs available in the stock items
            switch (GameManager.LevelData.levelNumber)
            {
                case 1:
                    bool grilling = CadacGriller.Instance.Grill(SausagePrefab);
                    break;
                case 2:
                    break;
            }
            //if (Level3Manager.isInitialized) // If this instance exists then level 3 is running
            //{
            //    if (Level3Manager.GrillUpgraded)
            //    {
            //        LargeGrill.Instance.PlaceHotDogOnLargeGrill();
            //    }
            //    else
            //    {
            //        SmallGrill.Instance.PlaceHotDogOnSmallGrill();
            //    }
            //}
            //else if (Level2Manager.isInitialized)
            //{
            //    if (Level2Manager.GrillUpgraded && Level2Manager.DeepFryUpgraded)
            //    { // If the grill has been upgraded to a double grill
            //        LargeGrill.Instance.PlaceHotDogOnLargeGrill();
            //    }
            //    else
            //    {
            //        SmallGrill.Instance.PlaceHotDogOnSmallGrill();
            //    }
            //}
            //else
            //{
            //    CadacGriller.Instance.Grill(SausagePrefab);
            //}

            // Play the animation for feedback
            anim.Play("hotDogsContainerAnim", 0);
            Sound_Manager.Instance.PlaySound("grab_item");
            sausageWidget.GetComponentInChildren<TextMeshProUGUI>().text = HotDogCount.ToString();
        }
        else
        {
            Sound_Manager.Instance.PlaySound("no_stock");
        }
    }
}
