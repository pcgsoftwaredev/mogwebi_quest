﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Sausage : MonoBehaviour
{

    private Camera sceneCamera;
    public GameObject smokeEffect;

    public enum SausageState
    {
        cooking,
        cooked,
        burnt,
        binned,
        picked
    }

    public SausageState state;

    //cooking variables
    private float ellapseTime;
    private bool isCountingDown;
    private Animator CookedAnimation;
    private int fryingSoundID;

    //grill location of sausage
    private Vector3 fryPosition;
    private Transform grillSpotTransform;


    //drag-drop variables
    private BoxCollider sausageCollider;
    private Vector3 screenPoint;
    private Vector3 position;
    private float dragOffset;
    private Rigidbody sausageRB;
    private GameObject targetBun;

    // Start is called before the first frame update
    void Start()
    {
        dragOffset = 20f;
        sceneCamera = GameObject.FindGameObjectWithTag("SceneCamera").gameObject.GetComponent<Camera>();
        grillSpotTransform = transform.parent;
        CookedAnimation = gameObject.GetComponent<Animator>();
        state = SausageState.cooking;

        sausageRB = gameObject.GetComponent<Rigidbody>();
        sausageCollider = gameObject.GetComponent<BoxCollider>();
        sausageCollider.enabled = false;
        fryingSoundID = Sound_Manager.Instance.PlaySound("sausage_frying");
        StartTimer();
    }

    // Update is called once per frame
    void Update()
    {

        if (isCountingDown)
        {
            ellapseTime += Time.deltaTime;

            if (ellapseTime > 3 && ellapseTime <= 8)
            { // Hotdog is cooked when it has been cooking for more than 5 seconds
                state = SausageState.cooked;
                sausageCollider.enabled = true;
                CookedAnimation.SetBool("cooked", true);
                gameObject.GetComponent<SpriteRenderer>().color = Color.Lerp(gameObject.GetComponent<SpriteRenderer>().color, new Color(0.8f, 0.8f, 0.8f, 1f), 3);
            }
            else if (ellapseTime > 8 && ellapseTime <= 10)
            { // Hotdog is burnt after 10 seconds
                state = SausageState.burnt;
                gameObject.GetComponent<SpriteRenderer>().color = Color.Lerp(gameObject.GetComponent<SpriteRenderer>().color, new Color(0.3f, 0.3f, 0.3f, 1f), 2);
                gameObject.GetComponent<BoxCollider>().enabled = false;
                smokeEffect.SetActive(true);
                CookedAnimation.SetBool("burnt", true);
            }
            else if (ellapseTime > 13)
            { // Hotdog is automatically moved to the bin after 1 seconds of being burnt
                state = SausageState.binned;
                smokeEffect.SetActive(false);
                Sound_Manager.Instance.PlaySound("trash_item");
                isCountingDown = false;
                Sound_Manager.Instance.StopSound(fryingSoundID);
                Destroy(gameObject);
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("HotDogBun"))
        {
            targetBun = collision.gameObject;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("HotDogBun"))
        {
            targetBun = null;
        }
    }

    private void OnMouseDown()
    {
        fryPosition = gameObject.transform.position;
        screenPoint = sceneCamera.WorldToScreenPoint(fryPosition);
        position = sceneCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y + dragOffset, screenPoint.z));

        if (state == SausageState.cooked)
        {
            Sound_Manager.Instance.PauseSound(fryingSoundID);
            gameObject.transform.SetParent(transform.root);
            gameObject.transform.position = position;
            isCountingDown = false;
        }
    }

    private void OnMouseDrag()
    {
        Vector3 newScreenPpoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);// current screen point
        position = sceneCamera.ScreenToWorldPoint(new Vector3(newScreenPpoint.x, newScreenPpoint.y + dragOffset, newScreenPpoint.z));
        sausageCollider.enabled = false;
        sausageRB.velocity = (position - gameObject.transform.position) * 50;
    }

    void OnMouseUp()
    { 
        if(targetBun != null)
        {
            if (targetBun.GetComponent<HotDog>().HotDogOrderState.Equals(HotDog.OrderState.Bun))
            {
                targetBun.transform.localScale = Vector3.one;
                targetBun.GetComponent<HotDog>().HotDogOrderState = HotDog.OrderState.PlainHotdog;
                targetBun.GetComponent<HotDog>().SetHotDogOrderSprite();
                Destroy(gameObject);
                return;
            }
        }

        Sound_Manager.Instance.UnPauseSound(fryingSoundID);
        gameObject.transform.SetParent(grillSpotTransform);
        gameObject.transform.position = fryPosition;
        isCountingDown = true;
        sausageRB.velocity = Vector3.zero;
    }


    // Simple timer
    public void StartTimer()
    {
        if (!isCountingDown)
        {
            isCountingDown = true;
            ellapseTime = 0;
        }
    }
}
