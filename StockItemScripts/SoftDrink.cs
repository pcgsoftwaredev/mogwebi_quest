﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoftDrink : MonoBehaviour
{
    private bool dragging = false;
    private bool draggedRight = false;
    private float distance;

    private Camera sceneCamera;
    private Vector3 originalPosition;

    // public AudioClip binDropSound;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        sceneCamera = GameObject.FindGameObjectWithTag("SceneCamera").gameObject.GetComponent<Camera>();
        //anim = gameObject.transform.GetChild(0).gameObject.GetComponent<Animator>();

        // Store the original position incase the object is dragged where there is no customer, so you can return it
        originalPosition = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        // If the bun is being dragged
        if (dragging)
        {
            Ray ray = sceneCamera.ScreenPointToRay(Input.mousePosition);
            Vector3 rayPoint = ray.GetPoint(distance);
            GetComponent<Transform>().position = rayPoint;
        }
    }

    private void OnMouseDown()
    {
        // Dont move the object if its empy or only has a bun
        distance = Vector3.Distance(transform.position, sceneCamera.transform.position);
        dragging = true;
    }

    void OnMouseUp()
    {
        dragging = false;
        // Debug.Log("But mouse went down");

        if (dragging == false)
        {
            if (!draggedRight)
            {
                transform.position = originalPosition;
            }
        }
    }
}
