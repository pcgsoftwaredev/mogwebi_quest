﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StockManager : Singleton<StockManager>
{
    public GameObject hotDogsServingTrayObj;
    public GameObject orderObj;

    public GameObject largeGrillerObj;
    public GameObject grillStandObj;
    public GameObject grillOilObj;
    public GameObject binObj;

    public bool thereIsACookingHotdog;

    public GameObject smoke; // Refence to the smoke particle effect
    public GameObject steam; // Refence to the smoke particle effect

    private Animator binAnim;

    // Audio clips
    public AudioClip bunDropSound;

    public enum TraySlotState
    {
        emptySlot,
        notEmpty,
    }

    public enum GrillSlotState
    {
        emptySlot,
        hasCookingHotDog,
        hasCookedHotDog,
        hasBurntHotDog,
    }


    // Start is called before the first frame update
    void Start()
    {
        binAnim = binObj.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void AnimateBin()
    {
        binAnim.Play("trashActive", 0);
    }
}
