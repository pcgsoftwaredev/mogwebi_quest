﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyFadeOut : MonoBehaviour
{

	public float fadeTimer;

	private Color mycolour;
	private float colourfade = 1;

	void Start()
	{
		mycolour = GetComponent<Image>().color;
		mycolour.a = 1f;
	}

	void Update()
	{

		fadeTimer -= Time.deltaTime;

		if (fadeTimer <= 0)
		{
			colourfade -= 0.03f;
			mycolour.a = colourfade;
			GetComponent<Image>().color = mycolour;
			if (colourfade <= 0)
			{
				Destroy(gameObject);
			}
		}

	}
}