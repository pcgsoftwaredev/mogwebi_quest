﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public static GameObject iconBeingDragged;
    Vector3 startPosition;
    Transform topLevel;//this is the storeBG GameObject
    Transform initialParent;
    float _zDistanceToCamera;
    float scaleAmount = 1.5f;

    void Start()
    {
        topLevel = transform.parent.parent.parent.parent.parent.transform;//modifying hierachy will break this if not adjusted accordingly
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        iconBeingDragged = gameObject;
        iconBeingDragged.layer = 2;
        initialParent = iconBeingDragged.transform.parent;
        iconBeingDragged.transform.SetParent(topLevel);
        iconBeingDragged.transform.localScale *= scaleAmount;
        startPosition = transform.position;
        _zDistanceToCamera = Mathf.Abs(startPosition.z - Camera.main.transform.position.z);

        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _zDistanceToCamera);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (transform.parent == topLevel)
        {
            transform.position = startPosition;
            iconBeingDragged.transform.SetParent(initialParent);
            iconBeingDragged.transform.localScale = Vector3.one;
        }
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }
}
