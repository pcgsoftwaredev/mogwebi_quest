﻿
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;


public class DropHandler : MonoBehaviour, IDropHandler
{

    public void OnDrop(PointerEventData eventData)
    {
        //only allow dropping item in slot if it is empty i.e: has no child objects
        if(transform.childCount == 0)
        {
            string id = DragHandler.iconBeingDragged.gameObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text;
            AddItemToCollection(id);
       
            DragHandler.iconBeingDragged.transform.SetParent(transform);
            DragHandler.iconBeingDragged.transform.localScale = Vector3.one;
            DragHandler.iconBeingDragged.transform.GetChild(0).localScale = Vector3.one;
        }
    }

    void AddItemToCollection(string id)
    {
        string[] itemID = id.Split('-');
        string collection = itemID[0];
        int index = int.Parse(itemID[1]);

        switch (collection)
        {
            case "ins":
                Debug.Log("INSURANCE product added to collection: Product index is "+index);
                break;
            case "inv":
                Debug.Log("INVESTMENT product added to collection: Product index is " + index);
                break;
            case "sav":
                Debug.Log("SAVINGS product added to collection: Product index is " + index);
                break;
            case "loan":
                Debug.Log("LOAN product added to collection: Product index is " + index);
                break;
        }
    }
}
