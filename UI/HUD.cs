﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUD : Singleton<HUD>
{
    public GameObject advisorBulb;

    public TMP_Text timerTxt;
    private int remainingTime;

    public Slider revenueSlider;
    public TMP_Text revenueTxt;
    private float currentRevenue;
    public static int MaxRevenue { get; set; }

    public GameObject customerServiceRatingPanel;
    public Sprite goldStar, emptyStar;
    private int currentCustomerServiceRating, ratingUpdates;
    public static int MaxCustomerSat { get; set; }

    public Slider finIQSlider;
    public TMP_Text finIQTxt;
    private float currentFinIQ;
    public static int MaxFinIQ { get;  set; }

    public TMP_Text customerCount;

    private bool timeAlmostUp;

    // Start is called before the first frame update
    void Start()
    {
        ratingUpdates = 0;
        timeAlmostUp = false;
        Initialise();
        StartCoroutine("TimeMonitor");
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTimer();
        UpdateRevenueUI();
        UpdateFinIQUI();
        UpdateRemainingCustomers();
        if (GameManager.ServiceRatingUpdated && GameManager.CurrentServiceRating > 0)
        {
            ratingUpdates++;
            UpdateCustomerServiceRatingUI();
            GameManager.ServiceRatingUpdated = false;
        }
        
    }

    //sets initial HUD values according to current level and challenge
    void Initialise()
    {
      
        currentRevenue = 0;
        currentFinIQ = 0;
        currentCustomerServiceRating = 0;

        UpdateTimer();
        UpdateRevenueUI();
        UpdateFinIQUI();
        UpdateCustomerServiceRatingUI();
    }


    void UpdateTimer()
    {
        remainingTime = (int)GameManager.RemainingTime;
        if (remainingTime >= 0)
        {
            int seconds = remainingTime % 60;
            int minutes = remainingTime / 60;
            string time = minutes + ":" + seconds;
            if (seconds < 10)
            {
                time = minutes + ":0" + seconds;
            }

            if (minutes == 0 && seconds == 10)
                timeAlmostUp = true;

            timerTxt.text = time;
        }
    }

    void UpdateRevenueUI()
    {
        currentRevenue = GameManager.CurrentRevenue;
        if (currentRevenue >= 0)
        {
            // Set the string value
            string revenueTxtStr = currentRevenue+"/"+ MaxRevenue;
            revenueTxt.text = revenueTxtStr;

            // Revenue value can exceed max revenue for challenge, but slider cannot
            if (currentRevenue <= MaxRevenue)
            {
                // Set the slider value
                revenueSlider.value = currentRevenue / MaxRevenue;
            }
        }
    }

    void UpdateCustomerServiceRatingUI()
    {
        currentCustomerServiceRating += GameManager.CurrentServiceRating;
        Debug.Log("New rating: " + currentCustomerServiceRating);
        if (currentCustomerServiceRating < 0 || currentCustomerServiceRating > 5)
            return;
        ResetRatingPanel();
        for (int i = 0; i < currentCustomerServiceRating; i++)
        {
            customerServiceRatingPanel.transform.GetChild(i).GetComponent<Image>().sprite = goldStar;
        }
    }

    void UpdateFinIQUI()
    {
        currentFinIQ = GameManager.CurrentFinIQ;
        // Set the string value
        finIQTxt.text = currentFinIQ + "/" + MaxFinIQ;

        // Set the slider value
        finIQSlider.value = currentFinIQ / MaxFinIQ;       
    }

    void UpdateRemainingCustomers(int bonus = 0)
    {
        if (bonus > 0)
        {
            //do something fancy when player gets bonus customers
        }
        customerCount.text = GameManager.RemainingCustomers.ToString();
    }

    void ResetRatingPanel()
    {
        for (int i = 0; i < 5; i++)
        {
            customerServiceRatingPanel.transform.GetChild(i).GetComponent<Image>().sprite = emptyStar;
        }
    }

    public void Advise()
    {
        StartCoroutine("FlashAdvisor");
    }

    IEnumerator TimeMonitor()
    {
        while (!timeAlmostUp)
            yield return null;
        timerTxt.color = Color.red;
        int tiktok = Sound_Manager.Instance.PlaySound("time_up");
        Sound_Manager.Instance.ReduceVolume();
        yield return new WaitForSeconds(10);
        Sound_Manager.Instance.StopSound(tiktok);
    }

    IEnumerator FlashAdvisor()
    {
        advisorBulb.SetActive(true);
        yield return new WaitForSeconds(1);
        advisorBulb.SetActive(false);
        yield return new WaitForSeconds(1);
        advisorBulb.SetActive(true);
        yield return new WaitForSeconds(1);
        advisorBulb.SetActive(false);
        yield return new WaitForSeconds(1);
        advisorBulb.SetActive(true);
        yield return new WaitForSeconds(5);
        advisorBulb.SetActive(false);
    }
}


