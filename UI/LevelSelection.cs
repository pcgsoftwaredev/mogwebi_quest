﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelection : MonoBehaviour
{
    public RectTransform[] sublevelPanels;
    [SerializeField] Button[] levelButtons;
    

    // Start is called before the first frame update
    void Start()
    {
        // Register to listen for state change events
        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);
        for (int i = 0; i < levelButtons.Length; i++)
        {
            Level level = JSONUtilScript.GetLevelData(i);
            bool locked = level.locked;
            if (!locked)
            {
                levelButtons[i].interactable = true;
                levelButtons[i].image.color = levelButtons[i].colors.selectedColor;
                levelButtons[i].transform.GetChild(2).gameObject.SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //method to decide what happens when the game changes state
    private void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState  previousState)
    {
        if (previousState.Equals(GameManager.GameState.PREGAME) && currentState.Equals(GameManager.GameState.LEVEL_RUNNING))
        {
            // Fadeout the level selection screen, or just disable it for now
            // This will only happen when the level is done loading
        }
    }

    public void Handle1()
    {
        Sound_Manager.Instance.PlayUISound("generic");
        GameManager.LevelData = JSONUtilScript.GetLevelData(0);
        TogglePanel(0);
    }

    public void Handle2()
    {
        Sound_Manager.Instance.PlayUISound("generic");
        GameManager.LevelData = JSONUtilScript.GetLevelData(1);
        TogglePanel(1);
    }

    public void Handle3()
    {
        Sound_Manager.Instance.PlayUISound("generic");
        GameManager.LevelData = JSONUtilScript.GetLevelData(2);
        TogglePanel(2);
    }

    public void Handle4()
    {
        Sound_Manager.Instance.PlayUISound("generic");
        GameManager.LevelData = JSONUtilScript.GetLevelData(3);
        TogglePanel(3);
    }

    public void Handle5()
    {
        Sound_Manager.Instance.PlayUISound("generic");
        GameManager.LevelData = JSONUtilScript.GetLevelData(4);
        TogglePanel(4);
    }

    public void Handle6()
    {
        Sound_Manager.Instance.PlayUISound("generic");
        GameManager.LevelData = JSONUtilScript.GetLevelData(5);
        TogglePanel(5);
    }

    public void Handle7()
    {
        Sound_Manager.Instance.PlayUISound("generic");
        GameManager.LevelData = JSONUtilScript.GetLevelData(6);
        TogglePanel(6);
    }

    public void Handle8()
    {
        Sound_Manager.Instance.PlayUISound("generic");
        GameManager.LevelData = JSONUtilScript.GetLevelData(7);
        TogglePanel(7);
    }

    public void Handle9()
    {
        Sound_Manager.Instance.PlayUISound("generic");
        GameManager.LevelData = JSONUtilScript.GetLevelData(8);
        TogglePanel(8);
    }


    void TogglePanel(int panelIndex)
    {
        DisableOtherSubPanels(panelIndex);
        if (sublevelPanels[panelIndex])
        {
            sublevelPanels[panelIndex].gameObject.SetActive(sublevelPanels[panelIndex].gameObject.activeSelf ? false : true);
        }
    }

    // Disables all other sublevel panels so that one at a time can be active
    void DisableOtherSubPanels(int currentPanelIndex)
    {
        for (int i = 0; i < sublevelPanels.Length; i++)
        {
            if (i != currentPanelIndex)
            {
                if (sublevelPanels[i]) //Deactivate any active panels
                    sublevelPanels[i].gameObject.SetActive(false);
            }
        }
    }

}
