﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    [SerializeField]private Button RestartButton;
    [SerializeField]private Button QuitLevelButton;
    [SerializeField]private Button QuitGameButton;
    [SerializeField]private Button ResumeBtn;


    // Start is called before the first frame update
    void Start()
    {
        ResumeBtn.onClick.AddListener(HandleResumeBtnClick);
        QuitLevelButton.onClick.AddListener(HandleQuitLevelBtnClick);
        QuitGameButton.onClick.AddListener(HandleQuitGameBtnClick);
        RestartButton.onClick.AddListener(HandleRestartBtnClick);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void HandleResumeBtnClick()
    {
        // Resume the game if in paused state
        GameManager.Instance.UnPauseGame();
    }

    void HandleQuitLevelBtnClick()
    {
        // Quit the level, unload the level
        GameManager.Instance.QuitLevel();
        
    }

    void HandleQuitGameBtnClick()
    {
        // Quit the game, unload the level
        GameManager.Instance.QuitGame();

    }

    void HandleRestartBtnClick()
    {
        // Restart the game, unload the level and then reload it
        GameManager.Instance.RestartLevel();
    }

    void HandleDismissBtnClick()
    {
        // Has the same function as resume, toggle pause
        GameManager.Instance.UnPauseGame();
    }
}
