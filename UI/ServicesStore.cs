﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using System;

public class ServicesStore : MonoBehaviour
{
    public GameObject loanCardPrefab;
    public GameObject investmentCardPrefab;
    public GameObject insuranceCardPrefab;
    public GameObject savingsCardPrefab;
    public GameObject productsPanel;

    private List<GameObject> instantiatedProductCards;
    //private LoanProduct[] loanProducts;
    //private InsuranceProduct[] insuranceProducts;
    //private InvestmentProduct[] investmentProducts;
    //private SavingsProduct[] savingsProducts;



    // Start is called before the first frame update
    void Start()
    {
        //instantiatedProductCards = new List<GameObject>();

        //loanProducts = JSONUtilScript.GetLoanProducts();

        //CreateLoanCards();
    }

    
    void CreateLoanCards()
    {
        ClearProductsPanel();
        GameObject loanCard;

        //foreach (LoanProduct loan in loanProducts)
        //{
        //    loanCard = Instantiate(loanCardPrefab, productsPanel.GetComponent<GridLayoutGroup>().transform) as GameObject;
        //    loanCard.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = loan.iconText;
        //    loanCard.transform.GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = loan.id;
        //    loanCard.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = loan.amount;
        //    loanCard.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = loan.interest;
        //    loanCard.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = loan.deposit;
        //    loanCard.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = loan.qualification;
        //}
    }

    void CreateInsuranceCards()
    {
        ClearProductsPanel();
        GameObject insuranceCard;

        //foreach (InsuranceProduct insurance in insuranceProducts)
        //{
        //    insuranceCard = Instantiate(insuranceCardPrefab, productsPanel.GetComponent<GridLayoutGroup>().transform) as GameObject;
        //    insuranceCard.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = insurance.iconText;
        //    insuranceCard.transform.GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = insurance.id;
        //    insuranceCard.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = insurance.name;
        //    insuranceCard.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = insurance.type;
        //    insuranceCard.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = insurance.premium;
        //    insuranceCard.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = insurance.cashback;
        //    insuranceCard.transform.GetChild(5).GetComponent<TextMeshProUGUI>().text = insurance.coverage;
        //    insuranceCard.transform.GetChild(6).GetComponent<Button>().onClick.AddListener(delegate { GetProductInfo(insurance.id); });
        //}
    }

    void CreateInvestmentCards()
    {
        ClearProductsPanel();
        GameObject investmentCard;

        //foreach (InvestmentProduct investment in investmentProducts)
        //{
        //    investmentCard = Instantiate(investmentCardPrefab, productsPanel.GetComponent<GridLayoutGroup>().transform) as GameObject;
        //    investmentCard.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = investment.iconText;
        //    investmentCard.transform.GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = investment.id;
        //    investmentCard.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = investment.name;
        //    investmentCard.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = investment.type;
        //    investmentCard.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = investment.minimumInvestment;
        //    investmentCard.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = investment.growthRate;
        //    investmentCard.transform.GetChild(5).GetComponent<TextMeshProUGUI>().text = investment.withdrawalLimit;
        //    investmentCard.transform.GetChild(6).GetComponent<TextMeshProUGUI>().text = investment.maturity;
        //    investmentCard.transform.GetChild(7).GetComponent<Button>().onClick.AddListener(delegate { GetProductInfo(investment.id); });
        //}
    }

    void CreateSavingsCards()
    {
        ClearProductsPanel();
        GameObject savingsCard;

        //foreach (SavingsProduct saving in savingsProducts)
        //{
        //    savingsCard = Instantiate(savingsCardPrefab, productsPanel.GetComponent<GridLayoutGroup>().transform) as GameObject;
        //    savingsCard.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = saving.iconText;
        //    savingsCard.transform.GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = saving.id;
        //    savingsCard.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = saving.name;
        //    savingsCard.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = saving.type;
        //    savingsCard.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = saving.minimumDeposit;
        //    savingsCard.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = saving.interest;
        //    savingsCard.transform.GetChild(5).GetComponent<TextMeshProUGUI>().text = saving.goals;
        //    savingsCard.transform.GetChild(6).GetComponent<Button>().onClick.AddListener(delegate { GetProductInfo(saving.id); });
        //}
    }

    void GetProductInfo(string id)
    {

    }

    public void CreateInsuranceProdCards()
    {
        ClearProductsPanel();
        //GameObject card;
        //foreach (InsuranceProd prod in insurances)
        //{
        //    card = Instantiate(insuranceCardPrefab, productsPanel.GetComponent<GridLayoutGroup>().transform) as GameObject;
        //    card.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = prod.title;
        //    card.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = prod.description;
        //    card.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "Cashback";
        //    card.transform.GetChild(3).GetChild(0).GetComponent<TextMeshProUGUI>().text = prod.cashBackAmount.ToString("F2");
        //}
    }


    void ClearProductsPanel()
    {
        foreach (Transform child in productsPanel.transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void Cleanup()
    {
       foreach(GameObject obj in instantiatedProductCards)
        {
            Destroy(obj);
        }
    }

    //create correctly formatted json files to be accessed via jsonhelper
    //void CreateJsonFiles()
    //{
    //    string loansJsonFile = Application.streamingAssetsPath + "/LoanProducts.json";
    //    string invJsonFile = Application.streamingAssetsPath + "/InvestmentProducts.json";
    //    string insJsonFile = Application.streamingAssetsPath + "/InsuranceProducts.json";

    //    loans = new LoanProd[10];
    //    insurances = new InsuranceProd[10];
    //    investments = new InvestmentProd[10];

    //    string loansJson = JsonHelper.ToJson(loans, true);
    //    string insuranceJson = JsonHelper.ToJson(insurances, true);
    //    string investmentsJson = JsonHelper.ToJson(investments, true);

    //    File.WriteAllText(loansJsonFile, loansJson);
    //    File.WriteAllText(invJsonFile, investmentsJson);
    //    File.WriteAllText(insJsonFile, insuranceJson);
    //}
}





