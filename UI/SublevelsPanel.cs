﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SublevelsPanel : MonoBehaviour
{
    private string _levelName;
    [SerializeField] private GameObject panelTitle;
    [SerializeField] private GameObject level1Revenue;
    [SerializeField] private GameObject level2Revenue;
    [SerializeField] private GameObject level3Revenue;

    [SerializeField]private Button level1Btn;
    [SerializeField]private Button level2Btn;
    [SerializeField]private Button level3Btn;

    private Button[] RoundButtons;

    // Start is called before the first frame update
    void Start()
    {
        RoundButtons = new Button[]{ level1Btn, level2Btn, level3Btn};

        level1Btn.onClick.AddListener(HandleBtnSublevel1);
        level2Btn.onClick.AddListener(HandleBtnSublevel2);
        level3Btn.onClick.AddListener(HandleBtnSublevel3);

        for (int i=0; i<RoundButtons.Length; i++)
        {
            LevelRound sublevel = GameManager.LevelData.rounds[i];
            bool locked = sublevel.locked;
            if (locked)
            {
                RoundButtons[i].interactable = false;
                RoundButtons[i].transform.GetChild(2).gameObject.SetActive(true);
            }
        }

        _levelName = GameManager.LevelData.levelName;
       
        SetupPanel(GameManager.LevelData);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetupPanel(Level level)
    {
        panelTitle.GetComponent<TextMeshProUGUI>().text = "Rounds";
        level1Revenue.GetComponent<TextMeshProUGUI>().text = "P" + level.rounds[0].targetRevenue.ToString();
        level2Revenue.GetComponent<TextMeshProUGUI>().text = "P" + level.rounds[1].targetRevenue.ToString();
        level3Revenue.GetComponent<TextMeshProUGUI>().text = "P" + level.rounds[2].targetRevenue.ToString();
    }

    void SetupSublevel(LevelRound sub)
    {
        GameManager.RemainingTime = sub.duration;
        GameManager.Instance.LoadLevel(_levelName);       
        UI_Manager.Instance.CloseLevelSelectionMenu();
    }


    // *** HANDLE SUBLEVEL BUTTONS***

    public void HandleBtnSublevel1()
    {
        Sound_Manager.Instance.PlayUISound("startRound");
        GameManager.CurrentRound = 0;
        LevelRound sub = GameManager.LevelData.rounds[0];
        SetupSublevel(sub);   
    }

    public void HandleBtnSublevel2()
    {
        Sound_Manager.Instance.PlayUISound("startRound");
        GameManager.CurrentRound = 1;
        LevelRound sub = GameManager.LevelData.rounds[1];
        SetupSublevel(sub);
    }

    public void HandleBtnSublevel3()
    {
        Sound_Manager.Instance.PlayUISound("startRound");
        GameManager.CurrentRound = 2;
        LevelRound sub = GameManager.LevelData.rounds[2];
        SetupSublevel(sub);
    }
}
