﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextAnimator : MonoBehaviour
{
	//list containint animator for each letter
	List<Animator> _animators;

	//delays between animations
	public float _delayBtwn = 0.2f;
	public float _delayEnd = 0.5f;
    
	// Start is called before the first frame update
	void Start()
    {
		_animators = new List<Animator>(GetComponentsInChildren<Animator>());
		StartCoroutine(DoAnimation());
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	IEnumerator DoAnimation()
	{
		while (true)
		{
			foreach (var animator in _animators)
			{
				animator.SetTrigger("DoTextAnimation");
				yield return new WaitForSeconds(_delayBtwn);
			}

			yield return new WaitForSeconds(_delayEnd);
		}

	}
}
