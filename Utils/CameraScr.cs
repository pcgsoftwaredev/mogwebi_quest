﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScr : MonoBehaviour
{
    [SerializeField]private SpriteRenderer tableBgObj;
    [SerializeField]private Transform tableBottomCenterMarker;
    [SerializeField]private float orthoSize;

    // Start is called before the first frame update
    void Start()
    {
        // The 0.1f removes the spaces at the edges, there seems to be some kind of padding there
        orthoSize = 0.5f * (tableBgObj.bounds.size.x / GetComponent<Camera>().aspect) - 0.1f;

        // Set the orthographic size
        GetComponent<Camera>().orthographicSize = orthoSize;

        GetComponent<Camera>().transform.position = new Vector3(
            tableBgObj.transform.position.x,
            GetComponent<Camera>().transform.position.y,
            GetComponent<Camera>().transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
