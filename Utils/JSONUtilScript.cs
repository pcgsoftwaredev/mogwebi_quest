﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;


public class JSONUtilScript : Singleton<JSONUtilScript>
{
    private static readonly string levelDataJSONFile = GetFilepath("LevelsInfoDataJSON.json");
    private static readonly string InsuranceTriviaJSONFile = GetFilepath("InsuranceTriviaJSON.json");
    private static readonly string loansJSONFile = GetFilepath("LoanProducts.json");
    private static readonly string investmentsJSONFile = GetFilepath("InvestmentProducts.json");
    private static readonly string insuranceJSONFile = GetFilepath("InsuranceProducts.json");
    private static readonly string savingsJSONFile = GetFilepath("SavingsProducts.json");
    private static readonly string adviceContentFile = GetFilepath("AdviceContent.json");
    private static readonly string levelEventsJSONpath = GetFilepath("LevelEventsJSONFile.json");
    private static string jsonString;
    private static string androidPath;


    // Start is called before the first frame update
    void Start()
    {

    }

    static string GetFilepath(string file)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            //copy over file from the StreamingAssets folder to PersistentDataPath
            string path = Path.Combine(Application.streamingAssetsPath + "/", file);
            if (path.Contains("://") || path.Contains(":///"))
            {


            }


            return Path.Combine(Application.persistentDataPath, file);
        }

        return Path.Combine(Application.streamingAssetsPath, file);
    }

    static IEnumerator GetPath(string filepath)
    {
        UnityEngine.Networking.UnityWebRequest www = UnityEngine.Networking.UnityWebRequest.Get(filepath);
        yield return www.Send();
        androidPath = www.downloadHandler.text;
    }

    // Get information about a certain level
    public static Level GetLevelData(int levelIndex)
    {
        jsonString = FileManagement.ReadFile<string>(levelDataJSONFile, fullPath: true);

        Level[] allLevels = JsonHelper.FromJson<Level>(jsonString);
        //Debug.Log(allLevels[levelIndex].levelName + " : Data Loaded Successfully!!");
        //Debug.Log("Sublevels = "+allLevels[levelIndex].Sublevels.Length);
        return allLevels[levelIndex];
    }

    //public static LoanProduct[] GetLoanProducts()
    //{
    //    jsonString = File.ReadAllText(loansJSONFile);
    //    return JsonHelper.FromJson<LoanProduct>(jsonString);
    //}

    //public static InsuranceProduct[] GetInsuranceProducts()
    //{
    //    jsonString = File.ReadAllText(insuranceJSONFile);
    //    return JsonHelper.FromJson<InsuranceProduct>(jsonString);
    //}

    //public static InvestmentProduct[] GetInvestmentProducts()
    //{
    //    jsonString = File.ReadAllText(investmentsJSONFile);
    //    return JsonHelper.FromJson<InvestmentProduct>(jsonString);
    //}

    //public static SavingsProduct[] GetSavingsProducts()
    //{
    //    jsonString = File.ReadAllText(savingsJSONFile);
    //    return JsonHelper.FromJson<SavingsProduct>(jsonString);
    //}

    public static Event GetLevelEvent(int eventNumber)
    {
        jsonString = File.ReadAllText(levelEventsJSONpath);
        Event[] events = JsonHelper.FromJson<Event>(jsonString);

        Event event_ = events[eventNumber - 1]; // Consider 0 indexing when coverting eventNumber to index on array

        Debug.Log("Reading Json, Event has " + event_.eventNumber + " and " + event_.eventTitle + " and " + event_.eventDescription);

        return event_;
    }

    public static AdvisorContent GetAdvisorContent(int level, int sublevel, int contentIndex)
    {
        jsonString = File.ReadAllText(adviceContentFile);
        AdvisorContent advisorContent = JsonUtility.FromJson<AdvisorContent>(jsonString);
        return advisorContent;
    }



}

[Serializable]
public class InsuranceTrivia
{
    public string question;
    public string[] options;
}


//[Serializable]
//public class LoanProduct
//{
//    public string id;
//    public string amount;
//    public string iconText;
//    public string qualification;
//    public string deposit;
//    public string interest;
//}


//[Serializable]
//public class InsuranceProduct
//{
//    public string id;
//    public int infoIndex;
//    public string name;
//    public string type;
//    public string iconText;
//    public string premium;
//    public string cashback;
//    public string coverage;
//    public int[] eventsCovered;
//}


//[Serializable]
//public class InvestmentProduct
//{
//    public string id;
//    public int infoIndex;
//    public string name;
//    public string type;
//    public string iconText;
//    public string minimumInvestment;
//    public string growthRate;
//    public string withdrawalLimit;
//    public string maturity;
//}

//[Serializable]
//public class SavingsProduct
//{
//    public string id;
//    public int infoIndex;
//    public string name;
//    public string type;
//    public string iconText;
//    public string minimumDeposit;
//    public string interest;
//    public string goals;
//}

[Serializable]
public class AdvisorContent
{
    public int level;
    public int sublevel;
    public string[] content;
}

[System.Serializable]
public class StockItem
{
    public string name;
    public int count;
}

[System.Serializable]
public class SublevelProducts
{
    public int productNum;
    public bool purchased;
}

[System.Serializable]
public class LevelRound
{
    public int round;
    public bool locked;
    public int targetRevenue;
    public int targetFinIQ;
    public int targetCustomerSatisfaction;
    public int duration;
    public int totalCustomers;
    public int customerFrequency;
    public StockItem[] stock;
    public string[] unlockables;
}

[System.Serializable]
public class Level
{
    public string levelName;
    public int levelNumber;
    public bool locked;
    public int customerSpawnInterval;
    public float customerTipsAmount;
    public float tipThreshold;
    public LevelRound[] rounds;
}

// To capture the events that occur in levels
[System.Serializable]
public class Event
{
    public int eventNumber;
    public string eventTitle;
    public string eventType;
    public string eventDescription;
    public string eventAlternateDescription;
    public int coveringProductNum;
    public int eventPenaltyFee;
}

public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}