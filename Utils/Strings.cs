﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: put this in Utils namespace
/// <summary>
/// Utility class to store dynamic strings that are set and used in scripts for GUI screens
/// </summary>
public static class Strings
{
    //character selection screen strings

    public static readonly string WELCOME_MESSAGE_NAME_ENTRY = "Welcome to MOGWEBI DASH!!\n\n" +
        "Enter your name in that box and press Continue to play.\n\n\n " +
        "You can skip this step by pressing the Skip button.";

    public static readonly string CS_SELECT_CHARACTER_TITLE = "Select Your Character";

    public static readonly string CS_ENTER_NAME_TITLE = "Enter Your Name";

    public static readonly string CS_ENTER_NAME_ERROR = "<color=red>Invalid Entry: Only letters and numbers allowed. Start with letter.";

    public static readonly string[] CS_LAUNCH_GAME = {
        "PLAY",

        "Select your favourite character.\n\n" +
        "Each character has great ambitions of wealth, however their fate is entirely in your hands!. \n\n" +
        "Will they end up rich or poor?\n" +
        "Press PLAY and let's find out." };
}
