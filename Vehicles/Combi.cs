﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combi : MonoBehaviour
{
    public GameObject[] vehicleImages;

    // private SoundManager soundManager;
    public AudioClip vehicleSound;
    public AudioSource vehicleSoundsSource;

    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        //soundManager = FindObjectOfType<SoundManager>();

        vehicleSoundsSource = GetComponent<AudioSource>();
        vehicleSoundsSource.clip = vehicleSound;

        StartCoroutine(AnimateVehicleCo());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator AnimateVehicleCo()
    {
        while (true)
        {
            yield return new WaitForSeconds(9.0f);

            // Play vehicle sound and animation
            vehicleSoundsSource.Play();
            anim.Play("CombiAnim", 0);
        }
    }
}
