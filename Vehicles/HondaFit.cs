﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HondaFit : MonoBehaviour
{
    public GameObject[] vehicleImages;

    // private SoundManager soundManager;
    public AudioClip vehicleSound;
    public AudioSource vehicleSoundsSource;

    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        //soundManager = FindObjectOfType<SoundManager>();

        vehicleSoundsSource = GetComponent<AudioSource>();
        vehicleSoundsSource.clip = vehicleSound;

        StartCoroutine(AnimateVehicleCo());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator AnimateVehicleCo()
    {
        while (true)
        {
            // Homdafit recurring time
            yield return new WaitForSeconds(15.0f);

            // Play vehicle sound and animation
            vehicleSoundsSource.Play();
            anim.Play("HondaFitAnim", 0);
        }
    }
}
