﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkSpaceSetup : MonoBehaviour
{
    [SerializeField]private Sprite blurredBgSprite;
    [SerializeField] private GameObject bgImage;

    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();

        // Wait for animation to finish and then change the background image to the blurred version
        StartCoroutine(BlurBgImageCo());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator BlurBgImageCo()
    {
        yield return new WaitForSeconds(6.0f);
        bgImage.GetComponent<SpriteRenderer>().sprite = blurredBgSprite;
    }
}
