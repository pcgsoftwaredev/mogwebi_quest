﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class AdviseBubble : Singleton<AdviseBubble>
{
    public GameObject scrollBar, acceptButton, dismissButton;
    public TMP_Text adviseContent, acceptButtonText;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    /// <summary>
    /// adviseText is an array of two elements
    /// [0] - title to display on button above the advisor, usually to accept some offer/advice
    /// [1] - the message to display in the advice panel
    /// </summary>
    /// <param name="adviseText"></param>
    public void AdviceMessage(string[] adviseText)
    {
        if (adviseText.Length > 2)
            Debug.LogError("ERROR: AdviseBubble.AdviseMessage array > 2");

        if (adviseText.Length == 2)
        {
            acceptButton.SetActive(true);
            acceptButtonText.text = adviseText[0];
            adviseContent.text = adviseText[1];
        }
        else
        {
            adviseContent.text = adviseText[0];
        }

        
        if (adviseContent.text.ToCharArray().Length > 120)
        {
            scrollBar.SetActive(true);
        }
    }

    //show or hide advice bubble dismiss button
    //dismiss button will be hidden for instances where the
    //advisor bubble should be constantly on-screen such as in tutorials
    //or in the character selection screen
    public void IsDismissable(bool dismissable)
    {
        dismissButton.SetActive(dismissable);
    }

    public void HandleAccept()
    {
        //do something if player accepts 
    }

    public void HandleDismiss()
    {
        //do something if player is punished for dismissing
    }

    public void Open()
    {
        transform.gameObject.GetComponent<Animator>().SetBool("Open", true);
        transform.gameObject.GetComponent<Animator>().SetBool("Close", false);
    }

    void Close()
    {
        transform.gameObject.GetComponent<Animator>().SetBool("Open", false);
        transform.gameObject.GetComponent<Animator>().SetBool("Close", true);
    }
}
