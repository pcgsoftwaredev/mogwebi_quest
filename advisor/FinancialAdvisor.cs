﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FinancialAdvisor : MonoBehaviour
{
    public TMP_Text adviseText;

    private Queue<string> adviseStrings;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        animator.SetBool("openAdvisor", true);
    }

    public void StartAdviceDialog(string[] content)
    {
        adviseStrings = new Queue<string>();
        foreach (string str in content)
        {
            adviseStrings.Enqueue(str);
        }
        DisplayNextAdvice();
    }

    public void DisplayNextAdvice()
    {
        if (adviseStrings.Count == 0)
        {
            EndDialog();
            return;
        }

        StopAllCoroutines();
        string nextAdvice = adviseStrings.Dequeue();
        if (nextAdvice.Contains("<"))
        {
            adviseText.text = nextAdvice;
        }
        else
        {
            StartCoroutine(TypeAdvise(nextAdvice));
        }
    }


    void EndDialog()
    {
        animator.SetBool("openAdvisor", false);
        Destroy(gameObject);
    }

    IEnumerator TypeAdvise(string advise)
    {
        adviseText.text = "";
        foreach (char character in advise.ToCharArray())
        {
            adviseText.text += character;
            yield return new WaitForSeconds(0.04f);
        }
    }
}
