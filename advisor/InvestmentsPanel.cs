﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InvestmentsPanel : MonoBehaviour
{
    public GameObject[] accounts;
    [SerializeField] TMP_Text NumberOfAccounts, PortfolioTotal;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Called whenever panel is set Active
    private void OnEnable()
    {
        SetValue(NumberOfAccounts, accounts.Length, false);
    }

    void SetValue(TMP_Text textElement, float value, bool showCurrency)
    {
        textElement.text = showCurrency ? "P " + value.ToString("N2") : textElement.text = value.ToString();
    }



}

