﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SavingsPanel : MonoBehaviour
{
    private List<GameObject> SavingsAccounts;
    [SerializeField] TMP_Text NumberOfAccounts, TotalSavings, InterestEarned;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void SetValue(TMP_Text textElement, float value, bool showCurrency)
    {
        textElement.text = showCurrency ? "P " + value.ToString("N2") : textElement.text = value.ToString();
    }



}

