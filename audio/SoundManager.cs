﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    public AudioSource efxSource; // For playing effects
    public AudioSource bgMusicSource; // For game music and looping background music
    public AudioSource bgSoundSourceTwo;
    public AudioSource oilFryingSoundSource;
    public AudioSource sourceOne;
    public AudioSource sourceTwo;
    public AudioSource sourceThree;

    [SerializeField] private AudioClip oilFryingSound;
    // Level background sound
    [SerializeField] public AudioClip levelBgSound;
    [SerializeField] public AudioClip oilSizzlingSound;
    [SerializeField] public AudioClip binDropSound;

    // Start is called before the first frame update
    void Start()
    {
        // Listen for game state changed
        if (GameManager.isInitialized)
            GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState previousState)
    {
        if (currentState == GameManager.GameState.PAUSED)
        { // Pause level gameplay sounds
            PauseGamePlaySounds();
            return;
        }

        if ((currentState == GameManager.GameState.LEVEL_RUNNING) && (previousState == GameManager.GameState.PAUSED))
        { // If transitioning from paused to resume, then resume sounds
            ResumeGamePlaySounds();
            return;
        }
    }

    public void MuteGameSounds()
    {
        if (sourceOne.isActiveAndEnabled)
            sourceOne.enabled = false;
        if (sourceTwo.isActiveAndEnabled)
            sourceTwo.enabled = false;
        if (sourceThree.isActiveAndEnabled)
            sourceThree.enabled = false;
        if (oilFryingSoundSource.isActiveAndEnabled)
            oilFryingSoundSource.enabled = false;
    }

    public void PauseGamePlaySounds()
    {
        if (sourceOne.isPlaying)
            sourceOne.Pause();
        if (sourceTwo.isPlaying)
            sourceTwo.Pause();
        if (sourceThree.isPlaying)
            sourceThree.Pause();
        if (oilFryingSoundSource.isPlaying)
            oilFryingSoundSource.Pause();
    }

    public void ResumeGamePlaySounds()
    {
        if (!sourceOne.isPlaying)
            sourceOne.UnPause();
        if (!sourceTwo.isPlaying)
            sourceTwo.UnPause();
        if (!sourceThree.isPlaying)
            sourceThree.UnPause();
        if (!oilFryingSoundSource.isPlaying)
            oilFryingSoundSource.UnPause();
    }

    public void PlaySingleSound(AudioClip clip)
    {
        efxSource.clip = clip;
        efxSource.Play();
    }

    public void PlayBackgroundSounds()
    {
        bgMusicSource.clip = levelBgSound;
        bgMusicSource.loop = true;
        bgMusicSource.Play();

        bgSoundSourceTwo.clip = oilSizzlingSound;
        bgMusicSource.loop = true;
        bgSoundSourceTwo.Play();
    }

    public void PlayOilFryingSound()
    {
        // Check if oil frying sound is already playing
        if (oilFryingSoundSource.isPlaying)
            return;

        oilFryingSoundSource.clip = oilFryingSound;
        oilFryingSoundSource.loop = true;
        oilFryingSoundSource.Play();
    }

    public void StopOilFryingSound()
    {
        if (ThereIsAFryingItem())
            return;

        oilFryingSoundSource.loop = false;
        oilFryingSoundSource.Stop();
    }

    private bool ThereIsAFryingItem()
    {

        return false;
    }

    public void PlayBinTrashSound()
    {
        if (!sourceOne.isPlaying)
        {
            sourceOne.clip = binDropSound;
            sourceOne.loop = false;
            sourceOne.Play();
        }
        else if (!sourceTwo.isPlaying)
        {
            sourceTwo.clip = binDropSound;
            sourceOne.loop = false;
            sourceTwo.Play();
        }
        else if (!sourceThree.isPlaying)
        {
            sourceThree.clip = binDropSound;
            sourceOne.loop = false;
            sourceThree.Play();
        }
    }

    public void PlayRandomSfxFromList(params AudioClip[] clips)
    {
        int randomIntIndex = Random.Range(0, clips.Length);
        efxSource.clip = clips[randomIntIndex];
        efxSource.Play();
    }
}
