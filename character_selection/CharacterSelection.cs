﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using TMPro;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using DanielLochner.Assets.SimpleScrollSnap;

public class CharacterSelection : MonoBehaviour
{
    SimpleScrollSnap sss;
    public TMP_InputField nameInputField;
    public TMP_Text title, errorText;
    public GameObject financialAdvisor, nameEntryPanel, characterSelectionCarousel;

    private Animator carouselAnim, nameEntryAnim;
    private string playerName;
    private bool nameEntryValid;

    private AdviseBubble advisor;

    // Start is called before the first frame update
    void Start()
    {
        sss = GetComponentInChildren<SimpleScrollSnap>();

        carouselAnim = characterSelectionCarousel.GetComponent<Animator>();
        nameEntryAnim = nameEntryPanel.GetComponent<Animator>();

        advisor = financialAdvisor.GetComponent<AdviseBubble>();
        playerName = "";
        nameEntryValid = false;
        title.text = Strings.CS_ENTER_NAME_TITLE;

        financialAdvisor.SetActive(true);
        advisor.IsDismissable(false);
        advisor.AdviceMessage(new string[] { Strings.WELCOME_MESSAGE_NAME_ENTRY });
        advisor.Open();

        nameInputField.onValueChanged.AddListener(ValidateNameEntry);

        //sss.onPanelChanged.AddListener(Swoosh);
        sss.onPanelSelected.AddListener(Swoosh);
    }

    // Update is called once per frame
    void Update()
    {

    }


    void ValidateNameEntry(string entry)
    {
        if (!Regex.Match(entry, @"^[a-zA-Z_]\w*\s*\w*(\.[a-zA-Z_]\w*)*$").Success)
        {
            if (string.IsNullOrEmpty(entry))
            {
                errorText.gameObject.SetActive(false);
                nameEntryValid = true;
                return;
            }

            nameEntryValid = false;
            errorText.gameObject.SetActive(true);
            errorText.text = Strings.CS_ENTER_NAME_ERROR;
        }
        else
        {
            nameEntryValid = true;
            errorText.gameObject.SetActive(false);
        }
    }

    public void HandleSkip()
    {
        Sound_Manager.Instance.PlayUISound("generic");
        //set enter name panel inactive
        nameEntryAnim.SetBool("slideout", true);

        //change page title to select character
        title.text = Strings.CS_SELECT_CHARACTER_TITLE;

        carouselAnim.SetBool("CarouselSlideIn", true);
        string greeting = "Hi!!\n";
        advisor.AdviceMessage(new string[] { Strings.CS_LAUNCH_GAME[0], greeting + Strings.CS_LAUNCH_GAME[1] });
        advisor.acceptButton.GetComponent<Button>().onClick.AddListener(HandlePlay);
    }

    void Swoosh()
    {
        Debug.Log("Current Panel: " + sss.NearestPanel);
        if (EventSystem.current.currentSelectedGameObject == sss.nextButton && sss.NearestPanel == sss.NumberOfPanels - 1)
            return;
        if (EventSystem.current.currentSelectedGameObject == sss.previousButton && sss.NearestPanel == 0)
            return;
        Sound_Manager.Instance.PlayUISound("swoosh");
    }

    public void HandleContinue()
    {
        Sound_Manager.Instance.PlayUISound("generic");
        //save text in text entry box as player name
        if (nameEntryValid)
        {
            nameEntryAnim.SetBool("slideout", true);

            title.text = Strings.CS_SELECT_CHARACTER_TITLE;
            playerName = nameInputField.text;
            GameManager.PlayerName = playerName;

            carouselAnim.SetBool("CarouselSlideIn", true);
            string greeting = "Hi " + playerName.Split(' ')[0] + "!\n";
            advisor.AdviceMessage(new string[] { Strings.CS_LAUNCH_GAME[0], greeting + Strings.CS_LAUNCH_GAME[1] });

            advisor.acceptButton.GetComponent<Button>().onClick.AddListener(HandlePlay);
        }

    }

    void HandlePlay()
    {
        int selectedCharacter = sss.CurrentPanel;
        Debug.Log("CS_HANDLE_PLAY: Selected Character is: " + selectedCharacter);
        GameManager.PlayerPersona = selectedCharacter;
        UI_Manager.Instance.DisplayLevelSelectionMenu();
    }

}
