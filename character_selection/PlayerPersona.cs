﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DanielLochner.Assets.SimpleScrollSnap;

public class PlayerPersona : MonoBehaviour, IPointerClickHandler
{
    SimpleScrollSnap sss;

    public void OnPointerClick(PointerEventData eventData)
    {
        //tellgamemanager that we are the chosen character
        GameManager.PlayerPersona = sss.CurrentPanel;
        Debug.Log("Selected player Persona is at index: "+sss.CurrentPanel);
    }


    // Start is called before the first frame update
    void Start()
    {
        sss = gameObject.GetComponentInParent<SimpleScrollSnap>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
