using System;using System.Collections;using TMPro;
using UnityEngine;using UnityEngine.UI;

public class Customer : MonoBehaviour{    private Vector3 _startPosition;    private CustomerManager.ServePoint _assignedServingPoint;    private bool isWaiting, spawnedLeft;

    //patience meter
    public Slider patienceMeter;    public Image patienceMeterColor;

    //money prefabs
    public GameObject coinsPrefab;
    public GameObject tipPrefab;
    private Animator CustomerAnimator;    private CapsuleCollider customerCollider;    private int customerService;
    private float lastPatienceLevel;

    public bool IsServed { get; set; }

    OrderManager currentOrder;


    //Start is called before the first frame update
    void Start()    {        _startPosition = transform.position;        customerCollider = gameObject.GetComponent<CapsuleCollider>();        CustomerAnimator = transform.GetComponent<Animator>();        isWaiting = false;        StartCoroutine(MoveToServingPoint(3f));        customerService = 0;        currentOrder = gameObject.GetComponent<OrderManager>();
    }

    private void Update()
    {
        
    }


    // ***************** METHODS ****************


    //sets the serving point assigned by the customer manager
    public CustomerManager.ServePoint AssignServingPoint    {        set => _assignedServingPoint = value;    }    void WalkOut()    {
		isWaiting = false;
        CustomerAnimator.SetBool("impatient", false);
        CustomerAnimator.SetBool("isWaiting", isWaiting);
		CustomerAnimator.SetBool("isWalkingOut", true);
        CustomerAnimator.SetBool("reachedWaitingPoint", false);
        CustomerAnimator.SetBool("walkout", true);

        gameObject.GetComponent<OrderManager>().Close();
        customerCollider.enabled = false;
        if (lastPatienceLevel > 0.7)
            customerService = 1;

        if (lastPatienceLevel < 0.5)
            customerService = -1;

        //decide whether with tip or not
        if (lastPatienceLevel >= GameManager.LevelData.tipThreshold)
        {
            GameObject tip = Instantiate(tipPrefab,new Vector2(transform.position.x + 1.5f, transform.position.y - 2f), Quaternion.identity) as GameObject;
            tip.GetComponentInChildren<TextMeshProUGUI>().text = "<color=green>TIP</color>  " + "P" + (GameManager.LevelData.customerTipsAmount * lastPatienceLevel ).ToString("N2");
            GameManager.CurrentRevenue += GameManager.LevelData.customerTipsAmount;
        }        if (_startPosition.x < 0)//walking out to the left
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
            gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
            gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().flipX = false;
        }        GameManager.CurrentServiceRating = customerService;        GameManager.ServiceRatingUpdated = true;        CustomerManager.Instance.DecrementCustomerCount();
        CustomerManager.Instance.FreeServingPoint(_assignedServingPoint.name);        StartCoroutine(MoveOutOfScene(4f));    }    private void OnCollisionEnter(Collision orderObj)
    {
        
        //Debug.Log("Received Item =  "+orderObj.gameObject.name.ToLower());

        //first check if game object that we collided with has a SpriteRender. All stock items should have one
        if (orderObj.gameObject.GetComponentInChildren<SpriteRenderer>() == null)
            return;

        //secondly check if it is amongst the items ordered by the customer
        //trim 1 appended to end of sprite name. The 1 is for hotdog sprites in a container
        string itemName = orderObj.gameObject.GetComponentInChildren<SpriteRenderer>().sprite.name.TrimEnd('1');
        
        if (!currentOrder.CustomerOrderItems.Contains(itemName))
            return;

        //passed all initial checks, so now process the orderItem
        ProcessOrderItem(orderObj.gameObject);
    }


    void ProcessOrderItem(GameObject orderItem)
    {
        
        string itemName = orderItem.GetComponentInChildren<SpriteRenderer>().sprite.name.ToLower();
		//remove image of item from order panel
		IsServed = currentOrder.RemoveServedItem(itemName);

        //add the items price to the revenue. Search it by sprite name in the ItemPrice list
        float revenue = currentOrder.ItemPrice(itemName);
        GameManager.CurrentRevenue += revenue;

        GameObject coinsObj = Instantiate(coinsPrefab,new Vector2(transform.position.x, transform.position.y - 1.5f), Quaternion.identity) as GameObject;
        coinsObj.GetComponentInChildren<TextMeshProUGUI>().text = "P" + revenue.ToString("N2");
        Tray.Instance.FreeSpot();
		Destroy(orderItem);
        CheckOrderCompleted();

    }

    private void CheckOrderCompleted()
    {
        if (IsServed)
        {
            isWaiting = false;
            CustomerAnimator.SetBool("impatient", false);
            CustomerAnimator.SetBool("reachedWaitingPoint", false);
            customerCollider.enabled = false;

            WalkOut();
        }
    }

    // ***************** COROUTINES ****************


    IEnumerator MoveToServingPoint(float walkSpeed)    {        customerCollider.enabled = false;        while (!(transform.position == _assignedServingPoint.position))        {            transform.position = Vector3.MoveTowards(transform.position, _assignedServingPoint.position, Time.deltaTime * walkSpeed);            yield return null;        }        //on exiting loop, customer has arrived at serving point        isWaiting = true;        customerCollider.enabled = true;        CustomerAnimator.SetBool("reachedWaitingPoint", true);

        StartCoroutine(StartPatienceMeter());        yield break;    }    IEnumerator MoveOutOfScene(float walkSpeed)
    {
        //change sort order so the customer walks out behind all the other waiting customers
        gameObject.GetComponent<SpriteRenderer>().sortingOrder -= 5;

        customerCollider.enabled = false;        yield return new WaitForSeconds(0.4f);        while (!(transform.position == _startPosition))        {            transform.position = Vector3.MoveTowards(transform.position, _startPosition, Time.deltaTime * walkSpeed);            yield return null;        }
        Destroy(gameObject);
    }    IEnumerator StartPatienceMeter()    {
        CustomerAnimator.SetBool("isWaiting", true);

        //change sortorder of waiting customer so that others don't pass in front of them
        gameObject.GetComponent<SpriteRenderer>().sortingOrder += 2;

        //show order panel
        gameObject.GetComponent<OrderManager>().Show();
        
        // Debug.Log("Patience Meter Couroutine started");
        if (patienceMeter != null)        {            float timeSlice = (patienceMeter.value / 2000f);            while (patienceMeter.value > 0 || isWaiting)            {                patienceMeter.value -= timeSlice;                patienceMeterColor.color = Color.Lerp(Color.red, Color.green, patienceMeter.value);

                 //WAITING PATIENTLY                if (patienceMeter.value <= 0.80 && patienceMeter.value >= 0.70)                {                                    }                //GETTING IMPATIENT                if (patienceMeter.value < 0.66 && patienceMeter.value > 0.33)                {
                    CustomerAnimator.SetBool("impatient", true);
                    
                }

                //GETTING ANGRY
                if (patienceMeter.value <= 0.33)                {
                    CustomerAnimator.SetBool("impatient", false);                                        
                }                //preparing to walk out                if(patienceMeter.value < 0.15)
                {
                    CustomerAnimator.SetBool("reachedWaitingPoint", false);
                    CustomerAnimator.SetBool("walkout", true);
                   
                }                //PATIENCE RUN OUT
                if (patienceMeter.value <= 0.1f)                {
                    
                    WalkOut();                    break;                }                lastPatienceLevel = patienceMeter.value;                yield return null;            }        }
        print("LAST PATIENCE: " + lastPatienceLevel);
        //  Debug.Log("Patience Meter Couroutine stopped");
        yield break;    }}