﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CustomerManager : Singleton<CustomerManager>
{

    public GameObject[] customerPrefabs;

    //customer spawning points
    public GameObject LeftSpawnPoint, RightSpawnPoint;
    Vector3 _leftSpawnPoint, _rightSpawnPoint;

    //serving spots for customers 
    public GameObject leftSpot, middleSpot, rightSpot;

    int _waitingCustomers;
    public bool _keepSpawning;
    int _maxConcurrentCustomers;
    int _maxCustomers;
    int _customersSpawnedAlready;
    int _spawnInterval; //the time delay between customer spawns in seconds

    public struct ServePoint
    {
        public string name;
        public Vector3 position;
        public bool isFree;

        public ServePoint(string name, Vector3 position, bool isFree)
        {
            this.name = name;
            this.position = position;
            this.isFree = isFree;
        }
    }

    public ServePoint left, middle, right;

    // Start is called before the first frame update
    void Start()
    {
        _waitingCustomers = 0;
        _customersSpawnedAlready = 0;
        _keepSpawning = true;
        _spawnInterval = GameManager.LevelData.customerSpawnInterval;

        //set left and right spawn positions
        _leftSpawnPoint = LeftSpawnPoint.transform.position;
        _rightSpawnPoint = RightSpawnPoint.transform.position;

        _maxConcurrentCustomers = GameManager.LevelData.rounds[GameManager.CurrentRound].customerFrequency;
        _maxCustomers = GameManager.LevelData.rounds[GameManager.CurrentRound].totalCustomers;
        GameManager.RemainingCustomers = _maxCustomers;

        left = new ServePoint(name: "left", position: leftSpot.transform.position, isFree: true);
        middle = new ServePoint(name: "middle", position: middleSpot.transform.position, isFree: true);
        right = new ServePoint(name: "right", position: rightSpot.transform.position, isFree: true);

        StartSpawningCustomers();
    }


    // ***************** METHODS ****************

    public void DecrementCustomerCount()
    {
        _waitingCustomers--;
        //Debug.Log("CUSTOMER COUNT DECREMENTED >>>>>>>>>>>>>>>>>>>>>>>> " + _customerCount);
    }

    //control when to spawn characters
    public void StartSpawningCustomers()
    {
        _keepSpawning = true;
        StartCoroutine(CustomerFactory());
    }

    public void StopSpawningCustomers()
    {
        _keepSpawning = false;
        StopCoroutine(CustomerFactory());
    }

    GameObject PickRandomCustomer()
    {
        customerPrefabs.Shuffle();
        int index = Random.Range(0, customerPrefabs.Length - 1);
        return customerPrefabs[index];
    }



    void SpawnCustomer()
    {
        GameObject customerGameObject;
        Vector3 spawnSide;
        ServePoint servingSpot;

        //try middle spot as default serving point first
        if (middle.isFree)
        {
            //random spawnside if middle spot is free
            spawnSide = (Random.Range(0, 11) < 5) ? _leftSpawnPoint : _rightSpawnPoint;
            servingSpot = middle;
            //Debug.Log("Assigned point: "+ servingSpot[0]);
            middle.isFree = false;
        }
        else
        {
            if (left.isFree)
            {
                spawnSide = _leftSpawnPoint;
                servingSpot = left;
                //Debug.Log("Assigned point: " + servingSpot[0]);
                left.isFree = false;
            }
            else
            {
                spawnSide = _rightSpawnPoint;
                servingSpot = right;
                //Debug.Log("Assigned point: " + servingSpot[0]);
                right.isFree = false;
            }
        }


        //instantiate customer from chosen side
        GameObject custPrefab = PickRandomCustomer();
        customerGameObject = Instantiate(custPrefab, spawnSide, custPrefab.transform.rotation) as GameObject;

        //assign the customer a waiting spot
        customerGameObject.GetComponent<Customer>().AssignServingPoint = servingSpot;

        //tell customer that they spawned from left, for animation control
        //flip the sprite to face in the direction of movement(it's facing right by default)
        if (spawnSide.Equals(_leftSpawnPoint))
        {
            customerGameObject.GetComponent<SpriteRenderer>().flipX = false;
            customerGameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().flipX = false;
        }
        else
        {
            customerGameObject.GetComponent<SpriteRenderer>().flipX = true;
            customerGameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().flipX = true;
        }

        _waitingCustomers++;
        _customersSpawnedAlready++;
        GameManager.RemainingCustomers--;
        //Debug.Log("CUSTOMER COUNT INCREMENTED >>>>>>>>>>>>>>>>>: "+ _customerCount);
    }


    //set serving point as available when customer leaves
    public void FreeServingPoint(string sp)
    {
        switch (sp)
        {
            case "left":
                left.isFree = true;
                break;
            case "middle":
                middle.isFree = true;
                break;
            case "right":
                right.isFree = true;
                break;

        }
    }

    public Vector3 LeftSpawner
    {
        get { return _leftSpawnPoint; }
    }

    public Vector3 RightSpawner
    {
        get { return _rightSpawnPoint; }
    }


    // ***************** COROUTINES ****************

    IEnumerator CustomerFactory()
    {
        Debug.Log("CUSTOMER FACTORY Couroutine started");
        while (_keepSpawning)
        {
            if (_waitingCustomers < _maxConcurrentCustomers)
            {
                SpawnCustomer();

                if (GameManager.RemainingCustomers == 0)
                    StopSpawningCustomers();

                yield return new WaitForSeconds(_spawnInterval);
            }
            else if (_customersSpawnedAlready >= _maxCustomers)
            {
                StopSpawningCustomers();
            }
            else
            {
                yield return null;
            }
        }
        Debug.Log("CUSTOMER FACTORY Couroutine stopped");
    }
}

public static class ListExtensions
{
    private static readonly System.Random random = new System.Random();

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = random.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}