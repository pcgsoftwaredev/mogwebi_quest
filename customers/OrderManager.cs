﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class OrderManager : MonoBehaviour
{
    public GameObject OrderPanel;
    public List<Sprite> StockItems;//will subset this array according to level/sublevel
    List<Sprite> orderItemsList;

    //panel display spots
    private GameObject topSpot, middleSpot, bottomSpot;
    private List<GameObject> OccupiedSpots;

    //Pricelist for all items in stock
    Dictionary<string, float> StockItemPrices = new Dictionary<string, float>
    {
        {"soft_drink" , 12.00f},
        {"fatcake", 8.00f},
        {"fries", 18.00f },
        {"plain_hotdog", 10.00f },
        {"mustard_hotdog", 15.00f },
        {"ketchup_hotdog", 15.00f },
        {"km_hotdog", 25.00f }
    };

    //List to store the random order generated for the customer
    public List<string> CustomerOrderItems;


    private void Start()
    {
        topSpot = OrderPanel.transform.GetChild(0).GetChild(1).GetChild(0).gameObject;
        middleSpot = OrderPanel.transform.GetChild(0).GetChild(1).GetChild(1).gameObject;
        bottomSpot = OrderPanel.transform.GetChild(0).GetChild(1).GetChild(2).gameObject;
        OccupiedSpots = new List<GameObject>();
        CustomerOrderItems = new List<string>();

        Order(GameManager.LevelData.levelNumber, GameManager.CurrentRound);
    }

    void Order(int level, int sublevel)
    {
        switch (level)
        {
            case 1:
                orderItemsList = StockItems.GetRange(0, 5);
                switch (sublevel)
                {
                    case 0:
                        MakeRandomOrder(lower: 0, count: 1, N: 1);
                        break;
                    case 1:
                        MakeRandomOrder(lower: 0, count: 4, N: 1);
                        break;
                    case 2:
                        MakeRandomOrder(lower: 0, count: 5, N: 1);
                        break;
                }
                break;
            case 2:
                orderItemsList = StockItems.GetRange(0, 6);
                switch (sublevel)
                {
                    case 0:
                        MakeRandomOrder(0, 5, 2);
                        break;
                    case 1:
                        MakeRandomOrder(0, 6, 2);
                        break;
                    case 2:
                        MakeRandomOrder(0, 6, 3);
                        break;
                }
                break;
            case 3:
                orderItemsList = StockItems.GetRange(0, 7);
                switch (sublevel)
                {
                    case 0:
                        MakeRandomOrder(4, 3, 3);
                        break;
                    case 1:
                        MakeRandomOrder(0, 6, 3);
                        break;
                    case 2:
                        MakeRandomOrder(0, 7, 2);
                        break;
                }
                break;
            case 4:
                orderItemsList = StockItems.GetRange(0, 5);
                switch (sublevel)
                {
                    case 0:

                        break;
                    case 1:

                        break;
                    case 2:

                        break;
                }
                break;
            case 5:
                orderItemsList = StockItems.GetRange(0, 5);
                switch (sublevel)
                {
                    case 0:

                        break;
                    case 1:

                        break;
                    case 2:

                        break;
                }
                break;
            case 6:
                orderItemsList = StockItems.GetRange(0, 5);
                switch (sublevel)
                {
                    case 0:

                        break;
                    case 1:

                        break;
                    case 2:

                        break;
                }
                break;
            case 7:
                orderItemsList = StockItems.GetRange(0, 5);
                switch (sublevel)
                {
                    case 0:

                        break;
                    case 1:

                        break;
                    case 2:

                        break;
                }
                break;
            case 8:
                orderItemsList = StockItems.GetRange(0, 5);
                switch (sublevel)
                {
                    case 0:

                        break;
                    case 1:

                        break;
                    case 2:

                        break;
                }
                break;
            case 9:
                orderItemsList = StockItems.GetRange(0, 5);
                switch (sublevel)
                {
                    case 0:

                        break;
                    case 1:

                        break;
                    case 2:

                        break;
                }
                break;
            case 10:
                orderItemsList = StockItems.GetRange(0, 5);
                switch (sublevel)
                {
                    case 0:

                        break;
                    case 1:

                        break;
                    case 2:

                        break;
                }
                break;
        }
    }

    /// <summary>
    /// Choose a max of N items from the subset of orderItemsList in range[lower,count]
    /// </summary>
    void MakeRandomOrder(int lower, int count, int N)
    {
        if (N > 3)
        {
            Debug.LogError("ERROR: CANNOT ORDER MORE THAN 3 ITEMS");
            return;
        }

        Random rand = new Random();
        Sprite[] items = { null, null, null };
        for (int i = 0; i < rand.Next(1, N); i++)
        {
            if (i <= orderItemsList.GetRange(lower, count).Count)
            {
                print("Order Item List Count: " + orderItemsList.GetRange(lower, count).Count);
                items[i] = Shuffle(orderItemsList.GetRange(lower, count))[i];
            }

        }
        SetOrderPanel(items[0], items[1], items[2]);
    }



    //shuffle order of items in list
    List<Sprite> Shuffle(List<Sprite> list)
    {
        Random rng = new Random();
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            Sprite temp = list[k];
            list[k] = list[n];
            list[n] = temp;
        }

        return list;
    }


    //set the sprites on orderpanel
    public void SetOrderPanel(Sprite item1, Sprite item2, Sprite item3)
    {
        string theOrder = "CUSTOMER ORDER:\n";
        if (item1 != null)
        {
            topSpot.SetActive(true);
            topSpot.transform.GetComponent<SpriteRenderer>().sprite = item1;
            theOrder += "1 = " + item1.name;
            CustomerOrderItems.Add(item1.name);
            OccupiedSpots.Add(topSpot);

        }

        if (item2 != null)
        {
            middleSpot.SetActive(true);
            middleSpot.transform.GetComponent<SpriteRenderer>().sprite = item2;
            theOrder += "\n2 = " + item2.name;
            CustomerOrderItems.Add(item2.name);
            OccupiedSpots.Add(middleSpot);

        }

        if (item3 != null)
        {
            bottomSpot.SetActive(true);
            bottomSpot.transform.GetComponent<SpriteRenderer>().sprite = item3;
            theOrder += "\n3 = " + item3.name;
            CustomerOrderItems.Add(item3.name);
            OccupiedSpots.Add(bottomSpot);

        }
        // Debug.Log(theOrder);
    }

    public bool RemoveServedItem(string itemName)
    {

        foreach (GameObject spot in OccupiedSpots)
        {
            if (spot.transform.GetComponent<SpriteRenderer>().sprite.name.Equals(itemName))
            {
                spot.transform.GetComponent<SpriteRenderer>().enabled = false;
                OccupiedSpots.Remove(spot);
                CustomerOrderItems.Remove(itemName);

                break;
            }
        }
        return OccupiedSpots.Count == 0;
    }


    //set order panel active
    public void Show()
    {
        OrderPanel.SetActive(true);
    }

    //set order panel inactive
    public void Close()
    {
        OrderPanel.SetActive(false);
    }

    public float ItemPrice(string item)
    {
        return StockItemPrices[item];
    }
}

