﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LevelGoal : Singleton<LevelGoal>
{

    [SerializeField] private Sprite empty_star, filled_star;
    [SerializeField] private TextMeshProUGUI revenueTarget, finIQTarget, title;
    [SerializeField] private GameObject rateBar;

    private int serviceTarget;
    public bool Fade { get; set; }
    public float fadeTimer;

    private Color mycolour;
    private float colourfade = 1;

    // Start is called before the first frame update
    void Start()
    {
        Fade = false;
        //take all the info from levelsJson and set them here
        LevelRound sublevel = GameManager.LevelData.rounds[GameManager.CurrentRound];
        revenueTarget.text = sublevel.targetRevenue.ToString();
        finIQTarget.text = sublevel.targetFinIQ.ToString();
        serviceTarget = sublevel.targetCustomerSatisfaction;

        if (GameManager.CurrentFinIQ >= sublevel.targetFinIQ)
            finIQTarget.text = "complete!";

        if (GameManager.CurrentRevenue >= sublevel.targetRevenue)
            revenueTarget.text = "complete!";

        SetServiceTargetBar();
        gameObject.GetComponent<Animator>().SetBool("showChallengeGoals", true);
        Sound_Manager.Instance.PlayUISound("roundGoals");
    }

    // Update is called once per frame
    void Update()
    {
        if (Fade)
            FadeOut();
    }

   void SetServiceTargetBar()
    {
        int stars = 0;
        for(int i=0; i < rateBar.transform.childCount; i++)
        {
            
            rateBar.transform.GetChild(i).GetComponent<Image>().sprite = filled_star;
            if (stars == serviceTarget)
                break;
            stars++;
        }
    }


    public void FadeOut()
    {
        fadeTimer -= Time.deltaTime;

        if (fadeTimer <= 0)
        {
            colourfade -= 0.03f;
            mycolour.a = colourfade;
            for (int i = 0; i < rateBar.transform.childCount; i++)
            {    
                rateBar.transform.GetChild(i).GetComponent<Image>().color = mycolour;
            }
            revenueTarget.color = mycolour;
            finIQTarget.color = mycolour;
            title.color = mycolour;
            gameObject.transform.GetChild(0).GetComponent<Image>().color = mycolour;
            if (colourfade <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

}
