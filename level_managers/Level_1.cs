﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level_1 : Singleton<Level_1>
{
    //global variables
    public float _remainingTime;
    private bool _isCountingDown, _gameOver, _levelComplete, _levelFailed, specialPause;

    public GameObject advisor, GlossaryPrefab, WOF_Prefab, goalsDisplay, levelCanvas, levelComplete, levelFailed, manageFin, roundComplete;
    private GameObject ChallengeGoals, lc, mf, _wheelOfFortune, _glossary;
    private Button advisorButton, goalsButton;
    public Dictionary<string, GameObject> stockCntWidgets;
    LevelRound levelRound;

   //prefabs
   [Header("Equipment Prefabs")]
    [Space(10)]
    [SerializeField] private GameObject ketchup;
    [SerializeField] private GameObject mustard;
    [SerializeField] private GameObject coolerBox;
    [SerializeField] private GameObject tableTop;
    [SerializeField] private GameObject sausageBox, bunsBox;

    [Header("Object Positions")]
    [SerializeField] private GameObject coolerPos;

    #region MONOBEHAVIOUR METHODS
    // Start is called before the first frame update
    void Start()
    {
        GameManager.LevelPrepared = false;
        _levelComplete = false;
        _levelFailed = false;

        //start the gameflow
        StartCoroutine("GameFlow");
    }

    // Update is called once per frame
    void Update()
    {

        _tick();

    }
    #endregion


    /// <summary>
    /// THE MASTER COROUTINE WHICH CONTROLS GAMEFLOW
    /// </summary>
    /// <returns></returns>
    #region GAMEFLOW_COROUTINE
    IEnumerator GameFlow()
    {
        yield return PrepareLevel();
        GameManager.LevelPrepared = true;
        GameManager.Instance.StartSpawningCustomers();
        StartTimer();

        while (!_levelComplete && !_levelFailed)
        {
            if (RoundComplete())
            {
                GameObject congrats;
                switch (levelRound.round)
                {
                    case 0:
                        GameManager.Instance.sub2.interactable = true;
                        GameManager.Instance.sub2.transform.GetChild(2).gameObject.SetActive(false);
                        
                        Sound_Manager.Instance.StopAllSounds();
                        Sound_Manager.Instance.StopMusic();
                        congrats = Instantiate(roundComplete);
                        Sound_Manager.Instance.PlaySound("round_complete");
                        yield return new WaitForSeconds(5);
                        Destroy(congrats);
                        GameManager.Instance.ResetTargets();

                        GameManager.Instance.QuitLevel();
                        break;
                    case 1:
                        GameManager.Instance.sub3.interactable = true;
                        GameManager.Instance.sub3.transform.GetChild(2).gameObject.SetActive(false);

                        Sound_Manager.Instance.StopMusic();
                        Sound_Manager.Instance.StopAllSounds();
                        congrats = Instantiate(roundComplete);
                        Sound_Manager.Instance.PlaySound("round_complete");
                        yield return new WaitForSeconds(5);
                        Destroy(congrats);
                        GameManager.Instance.ResetTargets();

                        GameManager.Instance.QuitLevel();
                        break;
                    case 2:
                        _levelComplete = true;
                        break;
                }
            }

            yield return null;
        }

        //gameover section
        if (_levelComplete)
        {
            Sound_Manager.Instance.PauseAllSounds();
            yield return GameCompleteAnim();
        }

        if (_levelFailed)
        {
            Sound_Manager.Instance.PauseAllSounds();
            yield return GameOverAnim();
        }
        yield break;
    }
    #endregion




    #region METHODS

    void Initialise()
    {
        advisorButton = UI_Manager.Instance.AdvisorButton;
        advisorButton.onClick.AddListener(HandleAdvisorTap);

        goalsButton = UI_Manager.Instance.goalsButton;
        goalsButton.onClick.AddListener(ShowGoals);


        // Register listener for game state changed
        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);

        _remainingTime = GameManager.RemainingTime;
        _isCountingDown = false;
    }


    private void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState previousState)
    {
        //do something when paused\unpaused
        if(currentState == GameManager.GameState.PAUSED)
        {
            Sound_Manager.Instance.PauseAllSounds();
            Sound_Manager.Instance.PlayMusic("pause");
        }else if(currentState == GameManager.GameState.LEVEL_RUNNING)
        {
            Sound_Manager.Instance.ResumeAllSounds();
            Sound_Manager.Instance.PlayMusic("level1");
        }
    }

    // level timing
    public void StartTimer()
    {
        if (!_isCountingDown)
        {
            _isCountingDown = true;
        }
    }

    private void _tick()
    {
        if (_isCountingDown)
        {
            _remainingTime -= Time.deltaTime;
            GameManager.RemainingTime = _remainingTime;

            if(_remainingTime <= 0.2f)
            {
                _levelFailed = true;
            }
        }

    }

    bool RoundComplete()
    {
        return GameManager.CurrentRevenue >= levelRound.targetRevenue && GameManager.CurrentFinIQ >= levelRound.targetFinIQ;
    }

    public void HandleAdvisorTap()
    {
        GameManager.Instance.PauseGame();
        //_isCountingDown = false;
        //CustomerManager.Instance.StopSpawningCustomers();
        advisor.SetActive(true);
        Sound_Manager.Instance.StopAllSounds();
        advisorButton.transform.GetChild(0).gameObject.SetActive(false);
    }
    #endregion

    public void AddMustard()
    {
        StartCoroutine("InstantiateMustardCo");
    }

    public void AddKetchup()
    {
        StartCoroutine("InstantiateKetchupCo");
    }

    public void AddCooler()
    {
        StartCoroutine("InstantiateCoolerCo");
    }

    public void GlossaryChallenge()
    {
        Sound_Manager.Instance.PlayMusic("quiz");
        _glossary = Instantiate(GlossaryPrefab, levelCanvas.transform);
        advisor.SetActive(false);
       
    }

    public void WheelOfFortune()
    {
        
        Sound_Manager.Instance.PauseAllSounds();
        GameManager.Instance.PauseGame();

        Sound_Manager.Instance.PlayMusic("quiz");
        _wheelOfFortune = Instantiate(WOF_Prefab, levelCanvas.transform);
        advisor.SetActive(false);
    }

    public void Exit()
    {
        
        Sound_Manager.Instance.ResumeAllSounds();
        Sound_Manager.Instance.PlayMusic("level1");
        GameManager.Instance.UnPauseGame();

        advisor.SetActive(false);
    }

    public void ShowGoals()
    {
        StartCoroutine("RoundGoals");
    }

    


    #region COROUTINES

    IEnumerator PrepareLevel()
    {
        levelRound = GameManager.LevelData.rounds[GameManager.CurrentRound];

        //Set level goals
        HUD.MaxRevenue = levelRound.targetRevenue;
        HUD.MaxFinIQ = levelRound.targetFinIQ;

        yield return new WaitForSeconds(4);
        Initialise();
        yield return RoundGoals();
        yield return new WaitForSeconds(2);
        Destroy(ChallengeGoals);
        sausageBox.SetActive(true);
        bunsBox.SetActive(true);
        

        //configure stock count HUD
        stockCntWidgets = new Dictionary<string, GameObject>();
        StockCountWidget.Instance.Reset();
        foreach (StockItem item in levelRound.stock)
        {
            if (StockCountWidget.Instance.stock.ContainsKey(item.name))
            {
                GameObject widget = StockCountWidget.Instance.CreateWidget(item.name, item.count);
                stockCntWidgets[item.name] = widget;
            }
        }

        Sound_Manager.Instance.PlayMusic("level1");
        if (GameManager.CurrentRound >= 1)
        {
            AddKetchup();
            AddMustard();

        }

        if(GameManager.CurrentRound == 2)
        {
            AddCooler();
        }
    }

    IEnumerator RoundGoals()
    {
        ChallengeGoals = Instantiate(goalsDisplay, levelCanvas.transform) as GameObject;
        yield return new WaitForSeconds(2.5f);
        LevelGoal.Instance.Fade = true;
    }

    IEnumerator GameCompleteAnim()
    {

        Sound_Manager.Instance.StopMusic();
        Sound_Manager.Instance.PauseAllSounds();
        UI_Manager.Instance.ToggleDarkOverlay();
        levelComplete.GetComponent<Animator>().SetBool("levelUp", true);
        lc = Instantiate(levelComplete, levelCanvas.transform) as GameObject;
        yield return new WaitForSeconds(5);
        GameManager.Instance.ResetTargets();
       
        Destroy(lc);
        GameManager.Instance.QuitLevel();
        UI_Manager.Instance.GetFeedBack();
    }

    IEnumerator GameOverAnim()
    {

        Sound_Manager.Instance.StopMusic();
        Sound_Manager.Instance.PauseAllSounds();
        UI_Manager.Instance.ToggleDarkOverlay();
        levelComplete.GetComponent<Animator>().SetBool("levelUp", true);
        lc = Instantiate(levelFailed, levelCanvas.transform) as GameObject;
        yield return new WaitForSeconds(5);
        GameManager.Instance.ResetTargets();
        Destroy(lc);
        GameManager.Instance.QuitLevel();
        UI_Manager.Instance.GetFeedBack();
    }

    IEnumerator InstantiateKetchupCo()
    {
       
        yield return new WaitForSeconds(0.5f);

        Vector2 ketchupPosition = new Vector2(tableTop.transform.position.x + 0.8f, tableTop.transform.position.y + 2.5f);
        GameObject ketchupObj = Instantiate(ketchup, ketchupPosition, transform.rotation) as GameObject;

        yield break;
    }

    IEnumerator InstantiateMustardCo()
    {

        // Create a lag between anims, for a nicer finish
        yield return new WaitForSeconds(1.5f);

        Vector2 mustardPosition = new Vector2(tableTop.transform.position.x - 0.8f, tableTop.transform.position.y + 2.5f);
        GameObject mustardObj = Instantiate(mustard, mustardPosition, transform.rotation) as GameObject;

        yield break;
    }

    IEnumerator InstantiateCoolerCo()
    {
        // Create a lag between anims, for a nicer finish
        yield return new WaitForSeconds(0.5f);
        Instantiate(coolerBox, coolerPos.transform.position, transform.rotation);
        yield break;
    }

    
    #endregion
}
