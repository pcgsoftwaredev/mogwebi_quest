﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level_2 : Singleton<Level_2>
{
    private float _remainingTime;
    private bool _roundFailed, _roundComplete;
    private Button advisorButton, goalsButton;
    private LevelRound levelRound;

    public Dictionary<string, GameObject> stockCntWidgets;

    #region MONOBEHAVIOUR_METHODS
    private void Start()
    {
        GameManager.LevelPrepared = false;
        _roundComplete = false;
        _roundFailed = false;
        StartCoroutine("GameFlow");
    }

    private void Update()
    {
        if (GameManager.LevelPrepared)
            TickTock();
    }
    #endregion

    #region METHODS

    //Level timer
    private void TickTock()
    {
        _remainingTime -= Time.deltaTime;
        if (_remainingTime <= 0.2f)
            _roundFailed = true;
    }

    //tra ck game state
    private void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState previousState)
    {
        switch (currentState)
        {
            case GameManager.GameState.LEVEL_RUNNING:
                Sound_Manager.Instance.ResumeAllSounds();
                Sound_Manager.Instance.PlayMusic("level2");
                break;
            case GameManager.GameState.PAUSED:
                Sound_Manager.Instance.PauseAllSounds();
                Sound_Manager.Instance.PlayMusic("pause");
                break;
        }
    }

    //check if player has achieved round goals
    private bool RoundComplete()
    {
        return GameManager.CurrentRevenue >= levelRound.targetRevenue && GameManager.CurrentFinIQ >= levelRound.targetFinIQ;
    }
    #endregion

    #region MASTER_GAMEFLOW_COROUTINE
    IEnumerator GameFlow()
    {
        yield return PrepareLevel();
        GameManager.Instance.StartSpawningCustomers();

        while(!_roundComplete && !_roundFailed)
        {
            yield return null;
        }
    }
    #endregion

    #region COROUTINES
    IEnumerator PrepareLevel()
    {
        //initialising 
        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);
        levelRound = GameManager.LevelData.rounds[GameManager.CurrentRound];
        _remainingTime = GameManager.RemainingTime;

        //Set level goals on HUD
        HUD.MaxRevenue = levelRound.targetRevenue;
        HUD.MaxFinIQ = levelRound.targetFinIQ;

        //delay for level intro animation
        yield return new WaitForSeconds(4);


        GameManager.LevelPrepared = true;
    }

    
    #endregion
}