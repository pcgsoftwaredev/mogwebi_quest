﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level3Manager : Singleton<Level3Manager>
{
    public GameObject advisorPanel;

    private float _remainingTime;
    private bool _isCountingDown, _gameOver, _gamePaused;
    private int _challenge;


    [Header("Equipment Sprites")]
    [Space(10)]
    [SerializeField] private Sprite coolerBoxSprite;
    [SerializeField] private Sprite largeGrillSprite;
    [SerializeField] private Sprite singleDeepFrySprite;
    [SerializeField] private Sprite doubleDeepFrySprite;

    [Header("Stock Prefabs")]
    [Space(10)]
    [SerializeField] private GameObject doughContainer;
    [SerializeField] private GameObject potatoesContainer;

    [Header("Equipment Prefabs")]
    [Space(10)]
    [SerializeField] private GameObject ketchup;
    [SerializeField] private GameObject mustard;
    [SerializeField] private GameObject coolerBox;
    [SerializeField] private GameObject singleDeepFryer;
    [SerializeField] private GameObject doubleDeepFryer;
    [SerializeField] private GameObject smallGrill;
    [SerializeField] private GameObject largeGrill;
    [SerializeField] private GameObject tableTop;

    [Header("Item Positions")]
    [Space(10)]
    [SerializeField] private Transform ketchupSpot;
    [SerializeField] private Transform mustardSpot;
    [SerializeField] private Transform smallGrillSpot;
    [SerializeField] private Transform largeGrillSpot;
    [SerializeField] private Transform coolerBoxSpot;
    [SerializeField] private Transform potatoesBoxSpot;

    public static bool CoolerPurchased { get; set; }
    public static bool GrillUpgraded { get; set; }
    public static bool DeepFryPurchased { get; set; }
    public static bool DeepFryUpgraded { get; set; }

    private Button advisorButton;

    #region MONOBEHAVIOUR METHODS
    // Start is called before the first frame update
    void Start()
    {
        advisorButton = UI_Manager.Instance.AdvisorButton;
        advisorButton.onClick.AddListener(HandleAdvisorTap);
        _remainingTime = GameManager.RemainingTime;
        SetupSublevel();


        // Instantiate the hotdog and ketchup objects
        StartCoroutine(InstantiateKetchupCo());
        StartCoroutine(InstantiateMustardCo());


        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);
    }

    // Update is called once per frame
    void Update()
    {
        _tick(); // Tick tock

        //_currentLevelName = GameManager._currentLevelName;
        //_currentSublevel = GameManager._sublevelChallenge;
    }
    #endregion



    #region GAMEFLOW COROUTINE
    #endregion

    #region METHODS
    private void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState previousState)
    {
        if (currentState.Equals(GameManager.GameState.PAUSED))
        {
            Debug.Log("GAME IS PAUSED!");
            Sound_Manager.Instance.PauseAllSounds();

        }

    }

    public void SetupSublevel()
    {
        StartCoroutine(SetupSublevelCon());
    }

    //level timing
    public void StartTimer()
    {
        if (!_isCountingDown)
        {
            _isCountingDown = true;
        }
    }

    private void _tick()
    {
        if (_isCountingDown)// && remainingTime > 0 : to avoid going negative
        {
            _remainingTime -= Time.deltaTime;
            GameManager.RemainingTime = _remainingTime;
        }
    }
    #endregion

    private IEnumerator SetupSublevelCon()
    { // Sets up sublevels 1 - 9, activates and diactivates, locks and unlocks items. Setsup the look feel and backgrounds
      // Check for upgrades, scene is created with some elements already here,  e.g cooler box, mustard and ketchup will already be in the scene


        yield return new WaitForSeconds(1.0f);

        Vector2 singleFryPosition = new Vector2(tableTop.transform.position.x - 5.5f, tableTop.transform.position.y + 2.5f);

        Vector2 doubleFryPosition = new Vector2(tableTop.transform.position.x - 5.5f, tableTop.transform.position.y + 2.5f);

        Vector2 doughContainerPosition = new Vector2(tableTop.transform.position.x - 2.8f, tableTop.transform.position.y + 2.0f);

        CoolerPurchased = true;
        if (CoolerPurchased)
        {
            StartCoroutine(InstantiateCoolerBoxCo());
        }

        DeepFryPurchased = true;
        DeepFryUpgraded = true;

        if (DeepFryPurchased)
        {
            if (DeepFryUpgraded)
            {
                GameObject doubleFeepFryItem = Instantiate(doubleDeepFryer, doubleFryPosition, transform.rotation);
                doubleFeepFryItem.transform.parent = tableTop.transform;
                GameObject doughContainerItem = Instantiate(doughContainer, doughContainerPosition, transform.rotation);
                doughContainerItem.transform.parent = tableTop.transform;

                StartCoroutine(InstantiatePotatoesBoxCo());
            }
            else
            {
                GameObject singleDeepFryerItem = Instantiate(singleDeepFryer, singleFryPosition, transform.rotation);
                singleDeepFryerItem.transform.parent = tableTop.transform;

                StartCoroutine(InstantiatePotatoesBoxCo());
            }
        }

        if (GrillUpgraded)
        {
            //if (SmallGrill.isInitialized)
            //    Destroy(SmallGrill.Instance.gameObject);

            //GameObject largerGrill = Instantiate(largeGrill, largeGrillPosition, transform.rotation);
            //largerGrill.transform.parent = tableTop.transform;
        }
        else
        {
            if (!CadacGriller.isInitialized)
            {
                StartCoroutine(InstantiateSmallGrillCo());
            }
        }
        GameManager.Instance.StartSpawningCustomers();
        StartTimer();
    }



    public void HandleAdvisorTap()
    {
        GameManager.Instance.PauseGame();
        UI_Manager.Instance.pauseMenu.SetActive(false);
        advisorPanel.SetActive(true);
    }

    public void UpgradeGrill()
    {
        advisorPanel.SetActive(false);
        if (CadacGriller.isInitialized)
            Destroy(CadacGriller.Instance.gameObject);
        Vector2 largeGrillPosition = new Vector2(tableTop.transform.position.x + 9.7f, tableTop.transform.position.y + 0.35f);
        GameObject largerGrill = Instantiate(largeGrill, largeGrillPosition, transform.rotation);
        largerGrill.transform.parent = tableTop.transform;
        GrillUpgraded = true;
    }

    #region COROUTINES
    IEnumerator InstantiateKetchupCo()
    {
        // wait for 6 the animation to finish before instantiating objecrt
        yield return new WaitForSeconds(2.5f);
        Instantiate(ketchup, ketchupSpot.position, transform.rotation);
    }

    IEnumerator InstantiateMustardCo()
    {
        // Create a lag between anims, for a nicer finish
        yield return new WaitForSeconds(3.0f);
        Instantiate(mustard, mustardSpot.position, transform.rotation);
    }

    IEnumerator InstantiateSmallGrillCo()
    {
        // Create a lag between anims, for a nicer finish
        yield return new WaitForSeconds(2.0f);
        GameObject smallerGrill = Instantiate(smallGrill, smallGrillSpot.position, transform.rotation);
        smallerGrill.transform.parent = tableTop.transform;
    }

    IEnumerator InstantiateCoolerBoxCo()
    {
        // Create a lag between anims, for a nicer finish
        yield return new WaitForSeconds(2.3f);
        GameObject coolerBoxItem = Instantiate(coolerBox, coolerBoxSpot.position, transform.rotation);
        coolerBoxItem.transform.parent = tableTop.transform;
    }

    IEnumerator InstantiatePotatoesBoxCo()
    {
        // Create a lag between anims, for a nicer finish
        yield return new WaitForSeconds(2.7f);
        GameObject potatoesContainerItem = Instantiate(potatoesContainer, potatoesBoxSpot.position, transform.rotation);
        potatoesContainerItem.transform.parent = tableTop.transform;
    }
    #endregion
}