﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AnswerSlot : MonoBehaviour, IDropHandler
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private string slotID;
    private bool slotsOccupied = false;

	public void OnDrop(PointerEventData eventData)
    {
        if(transform.childCount < 2)
        {
            slotID = gameObject.name;
            Debug.Log("Slot ID: " + slotID + " received Item ID: " + Draggable.itemBeingDragged.gameObject.name);
            slotsOccupied =true;
           
            Draggable.itemBeingDragged.gameObject.transform.SetParent(gameObject.transform);
            Draggable.itemBeingDragged.gameObject.transform.position = gameObject.transform.position;
            Draggable.itemBeingDragged.GetComponent<CanvasGroup>().blocksRaycasts = true;
        }

        
	}

	private void OnTransformChildrenChanged()
    {
        if (transform.childCount > 1)
        {
            GameObject answer = gameObject.transform.GetChild(1).gameObject;
            if (answer.GetComponentInChildren<TextMeshProUGUI>())
            {
                //play pop in sound
                //Debug.Log("Answer = " + answer.GetComponentInChildren<TextMeshProUGUI>().text);
                GlossaryChallenge.SetSlotAnswer(gameObject.name, gameObject);
                GlossaryChallenge.numOccupiedSlots ++;

                print(GlossaryChallenge.numOccupiedSlots);
                
            }       
            else
                Debug.Log("Recieved Gameobject has no textmesh");//sanity check
        }
        else
        {
            //play pop out sound
            gameObject.transform.GetChild(0).GetComponent<Image>().color = Color.white;
            Debug.Log("Child removed from "+gameObject.name);
            GlossaryChallenge.numOccupiedSlots --;
            print(GlossaryChallenge.numOccupiedSlots);
        
        }
    }
}
