﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public static GameObject itemBeingDragged;
    Vector3 startPosition;
    float _zDistanceToCamera;
    public static bool IsSlotted { get; set; }
   
    Transform initialParent, tempParent;

    void Start()
    {
        startPosition = Vector3.zero;
        IsSlotted = false;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        itemBeingDragged = gameObject;
        startPosition = transform.position;
        
        initialParent = itemBeingDragged.transform.parent;
        tempParent = initialParent.root;
        itemBeingDragged.transform.SetParent(tempParent);
        itemBeingDragged.transform.localScale = Vector3.one;
        
        //_zDistanceToCamera = Mathf.Abs(startPosition.z - Camera.main.transform.position.z);
        itemBeingDragged.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("Dragging: " + itemBeingDragged.name);
        itemBeingDragged.transform.position = Input.mousePosition;
            //new Vector3(Input.mousePosition.x, Input.mousePosition.y, _zDistanceToCamera);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        
       // itemBeingDragged.GetComponent<CanvasGroup>().blocksRaycasts = true;
        //Debug.Log("End Drag: " + itemBeingDragged.name);
        if(itemBeingDragged.transform.parent == tempParent)
        {
            itemBeingDragged.transform.position = startPosition;
            itemBeingDragged.transform.SetParent(initialParent);
            itemBeingDragged.GetComponent<CanvasGroup>().blocksRaycasts = true;
            itemBeingDragged = null;
        }
    }
}
