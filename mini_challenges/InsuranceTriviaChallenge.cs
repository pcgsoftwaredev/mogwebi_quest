﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DanielLochner.Assets.SimpleScrollSnap;
using TMPro;
using Random = System.Random;
using UnityEngine.UI;

public class InsuranceTriviaChallenge : MonoBehaviour
{
    public GameObject viewport;
    public GameObject triviaCardPrefab;

    //UI fields
    private Button[] buttons;
    public TMP_Text Timer, Score, Reward, QuestionsRemaining;
    private TMP_Text Question, optionA, optionB, optionC, optionD;

    private SimpleScrollSnap sss;
    private InsuranceTrivia[] InsuranceTriviaQuestions;

    private string _Question;
    private string[] _options;
    private int _correctOption;
    private Image _correctButtonImage;
    private int _reward;
    private int _score;
    private float _time;
    private bool _gameOver;


    // Start is called before the first frame update
    void Start()
    {
        sss = GetComponentInChildren<SimpleScrollSnap>();
        AddNextQuestion();
        Initialise();
        
    }

    void Initialise()
    {
        _time = 300;
        _score = 0;
        _gameOver = false;
        Score.text = _score + "/" + 9;
    }
     
    // Update is called once per frame
    void Update()
    {
        if (!_gameOver)
        {
            UpdateTimer();
        }
        
    }

    public void PlayerResponse()
    {
        string response = EventSystem.current.currentSelectedGameObject.name;
    
        Debug.Log("Answer Given: "+ response);
        switch (response)
        {
            case "A":
                CheckAnswer(0);

                break;
            case "B":
                CheckAnswer(1);
                break;
            case "C":
                CheckAnswer(2);
                break;
            case "D":
                CheckAnswer(3);
                break;

        }
    }

    void CheckAnswer(int ans)
    {
       
        //check answer 
        if(ans == _correctOption)
        {
            Debug.Log("CORRECT!");
            //play correct answer sound
            _score++;

            //change color
            Color32 green = new Color32(53, 203, 59,255);
            EventSystem.current.currentSelectedGameObject.GetComponent<Image>().color = green;
        }
        else
        {
            Debug.Log("WRONG!");
            //play wrong answer sound

            //change color
            Color32 red = new Color32(248, 123, 123, 255);
            EventSystem.current.currentSelectedGameObject.GetComponent<Image>().color = red;
            FlashCorrectAnswer();
        }
        //disable answer input
        foreach (Button button in buttons)
        {
            button.interactable = false;
        }

        UpdateScore();

    }

    void FlashCorrectAnswer()
    {
        StopCoroutine("Flash");
        StartCoroutine("Flash");
    }

    IEnumerator Flash()
    {
        int count = 6;
        _correctButtonImage.color = new Color32(53, 203, 59, 255);
        while (count > 0)
        {
            count --;
            switch (_correctButtonImage.color.a.ToString())
            {
                case "0":
                    _correctButtonImage.color = new Color(_correctButtonImage.color.r, _correctButtonImage.color.g, _correctButtonImage.color.b, 1);
                    //Play sound
                    yield return new WaitForSeconds(0.5f);
                    break;
                case "1":
                    _correctButtonImage.color = new Color(_correctButtonImage.color.r, _correctButtonImage.color.g, _correctButtonImage.color.b, 0);
                    //Play sound
                    yield return new WaitForSeconds(0.5f);
                    break;
            }
        }
        AddNextQuestion();
    }

    IEnumerator Confetti()
    {
        //play congrats sound
        //start confetti
        yield return new WaitForSeconds(2);
        // stop confetti
        yield break;
    }

    void AddNextQuestion()
    {
        _correctOption = 2;
        sss.AddToFront(triviaCardPrefab);
        GameObject nextQ = sss.Panels[0];

        //textfields for question and answers
        //TMP_Text Q = nextQ.transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>();
        //TMP_Text A = nextQ.transform.GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>();
        //TMP_Text B = nextQ.transform.GetChild(2).GetChild(1).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>();
        //TMP_Text C = nextQ.transform.GetChild(2).GetChild(2).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>();
        //TMP_Text D = nextQ.transform.GetChild(2).GetChild(3).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>();

        ////buttons
        Button buttonA = nextQ.transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Button>();
        Button buttonB = nextQ.transform.GetChild(2).GetChild(1).GetChild(0).GetComponent<Button>();
        Button buttonC = nextQ.transform.GetChild(2).GetChild(2).GetChild(0).GetComponent<Button>();
        Button buttonD = nextQ.transform.GetChild(2).GetChild(3).GetChild(0).GetComponent<Button>();
        buttons = new Button[] { buttonA, buttonB, buttonC, buttonD };
        foreach (Button b in buttons)
        {
            b.onClick.AddListener(PlayerResponse);
        }

        _correctButtonImage = buttons[_correctOption].GetComponent<Image>();

    }

 

    void UpdateScore()
    {
        Score.text = _score + "/" + 9;
    }

    void UpdateTimer()
    {
        _time -= Time.deltaTime;
        if (_time >= 0)
        {
            int seconds = (int)_time % 60;
            int minutes = (int)_time / 60;
            string time = minutes + ":" + seconds;
            if (seconds < 10)
                time = minutes + ":0" + seconds;

            Timer.text = time;
        }
        else
        {
            _gameOver = true;
        }

    }

    //shuffle order of items in list
    List<Sprite> Shuffle(List<Sprite> list)
    {
        Random rng = new Random();
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            Sprite temp = list[k];
            list[k] = list[n];
            list[n] = temp;
        }

        return list;
    }


        public void HandleQuit()
        {

        }

}


