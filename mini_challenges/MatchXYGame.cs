﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DanielLochner.Assets.SimpleScrollSnap;
using UnityEngine.UI;

public class MatchXY : MonoBehaviour
{
    /// <summary>
    /// HARDCODED DEMO VERSION FOR GLOSSARY CHALLENGE
    /// </summary>
    public GameObject challengePrefab, infoPanel;
    public TMP_Text challengeTitle, y1_text, y2_text,y3_text, x1_text, x2_text, x3_text, challengeCount;
    public GameObject slot1, slot2, slot3;
    private static GameObject s1, s2, s3;
    SimpleScrollSnap sss;

    private static string Answer1, Answer2, Answer3;
    private bool slotsFilled;
    private static int score;

    // Start is called before the first frame update
    void Start()
    {
        sss = GetComponentInChildren<SimpleScrollSnap>();

        //answers for demo. Realistically these will be loaded from the JSON 
        Answer1 = "insurance";
        Answer2 = "life assurance";
        Answer3 = "risk";

        s1 = slot1;
        s2 = slot2;
        s3 = slot3;
        score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (SlotsFilled())
        {
            GameManager.Instance.UnPauseGame();
            Destroy(gameObject);
        }
            
    }

    private static bool SlotsFilled()
    {
        return s1.transform.childCount > 1 && s2.transform.childCount > 1 && s3.transform.childCount > 1;
    }

    public static void SetSlotAnswer(string slot, GameObject obj)
    {
        switch (slot)
        {
            case "slot1":
                ValidateAnswer(Answer1, obj);
                break;
            case "slot2":
                ValidateAnswer(Answer2, obj);
                break;
            case "slot3":
                ValidateAnswer(Answer3, obj);
                break;
        }
    }

    private static void ValidateAnswer(string correctAnswer, GameObject obj)
    {
        Color wrong = Color.red;
        Color correct = Color.green;
        string answer = obj.GetComponentInChildren<TextMeshProUGUI>().text;
        if (answer.ToLower().Equals(correctAnswer))
        {
            //do sounds and animation for correct and increase FinIQ or whatever
            Sound_Manager.Instance.PlaySound("correct");
            score += 5;
            obj.transform.GetChild(0).GetComponent<Image>().color = correct;
        }
        else
        {
            //do sounds and animation for wrong
            Sound_Manager.Instance.PlaySound("no_stock");
            obj.transform.GetChild(0).GetComponent<Image>().color = wrong;

        }

        if (SlotsFilled())
        {
            GameManager.CurrentFinIQ = score;
        }

    }

    public void QuitGame()
    {
        GameManager.CurrentFinIQ += score;
        GameManager.Instance.UnPauseGame();
        Destroy(gameObject);
    }

    public void ToggleInfoPopup()
    {
        if (infoPanel.activeSelf)
        {
            infoPanel.SetActive(false);
        }
        else
        {
            infoPanel.SetActive(true);
        }
    }

    public void LoadNextChallenge()
    {
       
    }
}
