﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Text.RegularExpressions;

public class FinanceManagement : Singleton<FinanceManagement>
{
    [SerializeField] TMP_Text NetWorth;
    [SerializeField] GameObject[] networthArrows;//[0] up and [1]down

    //LEVEL SUMMARY PARAMETERS
    [SerializeField] GameObject summaryPanel, savingsPanel, investmentsPanel, debtsPanel;

    [SerializeField] Button savingsTab, investmentsTab, debtsTab;

    [SerializeField] GameObject responsibilitiesField, debtsField;

    /// <summary>
    /// [0] - sales
    /// [1] - savings interest
    /// [2] - investments
    /// [3] - cashbacks
    /// [4] - expenses
    /// [5] - responsibilities
    /// [6] - debts
    /// [7] - taxes
    /// </summary>
    [SerializeField] TMP_Text[] AmountFields;
    [SerializeField] GameObject[] Tips;

    [SerializeField] TMP_Text NetIncome, CashIn, CashOut;

    [SerializeField] GameObject[] tabIndicators;

    private Button[] buttons;

    // Start is called before the first frame update
    void Start()
    {
        summaryPanel.SetActive(true);
        savingsPanel.SetActive(false);
        investmentsPanel.SetActive(false);
        debtsPanel.SetActive(false);

        buttons = new Button[] { savingsTab, investmentsTab, debtsTab };

        //TESTING
        FinMathUtil.Responsibilities = new Dictionary<string, float>() { { "School Fees", 12707.90f }, { "Granny", 155.10f } };
        FinMathUtil.Investments = 900.90f;
        FinMathUtil.Sales = 1688.10f;


        StartCoroutine(SetSummaryData());
    }



    //switches "tab" ON and "defaultTab" OFF or vice-versa
    public bool ToggleTab(GameObject tab, GameObject defaultTab)
    {
        tab.SetActive(!tab.activeSelf);
        defaultTab.SetActive(!tab.activeSelf);

        //toggle tab indicators as well
        tabIndicators[0].SetActive(savingsPanel.activeSelf);
        tabIndicators[1].SetActive(investmentsPanel.activeSelf);
        tabIndicators[2].SetActive(debtsPanel.activeSelf);
        return tab.activeSelf;
    }

    void ResetTabColors()
    {
        ColorBlock colors;
        foreach (Button tab in buttons)
        {
            colors = tab.colors;
            colors.normalColor = Color.white;
            tab.colors = colors;
        }
    }

    void SetTabActive(string tab)
    {
        Button button = null;
        GameObject panel = null;
        ColorBlock colors;

        switch (tab)
        {
            case "savings":
                button = savingsTab;
                panel = savingsPanel;
                investmentsPanel.SetActive(false);
                debtsPanel.SetActive(false);
                break;
            case "investments":
                button = investmentsTab;
                panel = investmentsPanel;
                savingsPanel.SetActive(false);
                debtsPanel.SetActive(false);
                break;
            case "debts":
                button = debtsTab;
                panel = debtsPanel;
                savingsPanel.SetActive(false);
                investmentsPanel.SetActive(false);
                break;
        }

        //reset all tab colors
        ResetTabColors();

        //change normal color of active tab to make it appear always selected
        colors = button.colors;
        colors.normalColor = colors.selectedColor;
        button.colors = colors;

        bool activated = ToggleTab(panel, summaryPanel);
        if (activated)
        {
            EventSystem.current.SetSelectedGameObject(button.gameObject);
            button.Select();
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(null);
            ResetTabColors();
        }
    }

    //HANDLE TAB CLICKS
    public void HandleSavingsTabClick()
    {
        SetTabActive("savings");

    }

    public void HandleInvestmentsTabClick()
    {
        SetTabActive("investments");

    }

    public void HandleDebtsTabClick()
    {
        SetTabActive("debts");

    }

    public void HandleContinue()
    {
        UI_Manager.Instance.ToggleDarkOverlay();
        UI_Manager.Instance.GetFeedBack();
        Destroy(gameObject);
    }

    // Set UI Values
    IEnumerator SetSummaryData()
    {
        yield return StartCoroutine(CashInAnimation());
        yield return StartCoroutine(CashOutAnimation());
        yield return StartCoroutine(NettIncomeAnimation());
        yield return StartCoroutine(NetWorthAnimation());
        // StopAllCoroutines();
    }

    IEnumerator Count(TMP_Text obj, float initial, float target)
    {
        float current = initial;
        while (Math.Abs(current - target) > 0.05)
        {
            if (initial < target)
            {
                current += Time.deltaTime * 1.5f * (target - initial);
                if (current >= target)
                    current = target;
            }
            else
            {
                current -= Time.deltaTime * (initial - target);
                if (current <= target)
                    current = target;
            }
            obj.text = current.ToString("N2");
            Sound_Manager.Instance.PlayUISound("click");
            yield return null;

        }
    }

    IEnumerator CashInAnimation()
    {
        StartCoroutine(Count(AmountFields[0], 0, FinMathUtil.Sales));
        StartCoroutine(Count(AmountFields[1], 0, FinMathUtil.SavingsInterest));
        StartCoroutine(Count(AmountFields[2], 0, FinMathUtil.Investments));
        StartCoroutine(Count(CashIn, 0, FinMathUtil.CashInTotal()));
        yield return new WaitForSeconds(1);
    }

    IEnumerator CashOutAnimation()
    {
        StartCoroutine(Count(AmountFields[3], 0, FinMathUtil.ExpensesTotal()));

        //only show responsibilities and debts fields if they are > 0
        if (FinMathUtil.ResponsibilitiesTotal() > 0f)
        {
            responsibilitiesField.SetActive(true);
            StartCoroutine(Count(AmountFields[4], 0, FinMathUtil.ResponsibilitiesTotal()));
        }

        if (FinMathUtil.DebtsTotal() > 0f)
        {
            debtsField.SetActive(true);
            StartCoroutine(Count(AmountFields[5], 0, FinMathUtil.DebtsTotal()));
        }

        StartCoroutine(Count(CashOut, 0, FinMathUtil.CashOutTotal()));
        yield return new WaitForSeconds(1);
    }


    IEnumerator NettIncomeAnimation()
    {
        StartCoroutine(Count(NetIncome, 0, FinMathUtil.NettIncome()));
        yield return new WaitForSeconds(2);
    }

    IEnumerator NetWorthAnimation()
    {
        NetWorth.text = Regex.Replace(NetWorth.text, "[^0-9]", "");
        int prevNetWorth = int.Parse(NetWorth.text);
        int updated = prevNetWorth + (int)FinMathUtil.NettIncome();
        if (updated < prevNetWorth)
        {
            networthArrows[0].SetActive(false);
            networthArrows[1].SetActive(true);
        }
        else
        {
            networthArrows[0].SetActive(true);
            networthArrows[1].SetActive(false);
        }
        StartCoroutine(Count(NetWorth, prevNetWorth, updated));
        yield return null;
    }

}

/// <summary>
/// Handles all calculations and sets data values for Finances Management interface
/// </summary>
public static class FinMathUtil
{
    public static float Sales { get; set; }
    public static float SavingsInterest { get; set; }
    public static float Investments { get; set; }
    public static float Cashbacks { get; set; }


    public static Dictionary<string, float> Expenses { get; set; }
    public static Dictionary<string, float> Responsibilities { get; set; }
    public static Dictionary<string, float> Debts { get; set; }


    //CALCULATE TOTALS
    public static float ExpensesTotal()
    {
        if (Expenses.IsNullOrEmpty())
            return 0f;

        float total = 0f;
        foreach (float expense in Expenses.Values)
        {
            total += expense;
        }

        return total;
    }

    public static float ResponsibilitiesTotal()
    {
        if (Responsibilities.IsNullOrEmpty())
        {

            return 0f;
        }


        float total = 0f;
        foreach (float res in Responsibilities.Values)
        {
            total += res;
        }

        return total;
    }

    public static float DebtsTotal()
    {
        if (Debts.IsNullOrEmpty())
            return 0f;

        float total = 0f;
        foreach (float debt in Debts.Values)
        {
            total += debt;
        }

        return total;
    }

    //CALCULATE CASH IN TOTAL
    public static float CashInTotal()
    {
        return Sales + Investments + SavingsInterest;
    }

    //CALCULATE CASH OUT TOTAL
    public static float CashOutTotal()
    {
        return ExpensesTotal() + ResponsibilitiesTotal() + DebtsTotal();
    }

    //NETT INCOME
    public static float NettIncome()
    {
        return CashInTotal() - CashOutTotal();
    }

    //helper to check if dictionary is null or empty
    private static bool IsNullOrEmpty(this IDictionary Dictionary)
    {
        return (Dictionary == null || Dictionary.Count < 1);
    }
}
