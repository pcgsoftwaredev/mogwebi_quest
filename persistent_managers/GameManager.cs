﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System;

// For dispatching messages about state changes in the game. The arguments are <incomingGameState, previousGameState>
[Serializable]
public class GameStateEvents : UnityEvent<GameManager.GameState, GameManager.GameState> { }

public class GameManager : Singleton<GameManager>
{
    public Button sub2, sub3;

    public static Level LevelData { get; set; }
    public static int CurrentRound { get; set; }
    public static float CurrentRevenue { get; set; }
    public static int CurrentFinIQ { get; set; }
    public static int CurrentServiceRating { get; set; }
    public static bool ServiceRatingUpdated { get; set; }
    public static bool LevelPrepared { get; set; }
    public static float RemainingTime { get; set; }
    public static int RemainingCustomers { get; set; }
    public static int PlayerPersona { get; set; }
    public static string PlayerName { get; set; }

    // For the state machine, the different states the game will go through
    public enum GameState
    {
        PREGAME,
        LEVEL_RUNNING,
        PAUSED,
        LEVEL_COMPLETE,
        ROUND_COMPLETE,
        GAME_OVER
    }

    public GameState CurrentGameState { get; private set; }

    public GameStateEvents OnGameStateChanged;

    public GameObject CustomerManagerPrefab;
    private GameObject _customerManager;


    // Will contaiin a list of system prefabs. These are the prefabs we want to create in the scope of the boot scene
    public GameObject[] SystemPrefabs;

    // A list of the system prefabs after they have been created. Prefabs that have been created that you want the system to keep track of them
    // so that we can clean them up at a later time, its our responbility to clean them up and avoid memory leaks etc
    private List<GameObject> _instancedSystemPrefabs;

    // Keep track of async operations for example so you dont load 2 levels simultatiusly or to use in a loading screen
    List<AsyncOperation> _loadOperations;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        CurrentGameState = GameState.PREGAME;

        // Instantiate any persistant system that needs to be created with the game manager e.g. audio manager
        _instancedSystemPrefabs = new List<GameObject>();
        InstantiateSystemPrefabs();

        // When a load operation is initiated it is added to the list, when its done its removed
        _loadOperations = new List<AsyncOperation>();

        ServiceRatingUpdated = false;

        Debug.Log("Game Manager Started");
    }

    private void Update()
    {
        if (CurrentGameState == GameState.PREGAME)
            return;

        if (Input.GetKeyDown(KeyCode.Escape))
        { // Listen for backpress or escape key press
            if (CurrentGameState == GameState.PAUSED)
                UnPauseGame();
            else
                PauseGame();
        }
    }

    // Updates the state, takes an incoming state
    void UpdateState(GameState incomingState)
    {
        GameState previousState = CurrentGameState;
        CurrentGameState = incomingState;

        switch (CurrentGameState)
        {
            case GameState.PREGAME:
                Time.timeScale = 1.0f;
                break;
            case GameState.LEVEL_RUNNING:
                Time.timeScale = 1.0f;
                break;
            case GameState.PAUSED:
                // Pause the game by setting frame rate to zero
                Time.timeScale = 0.0f;
                break;
        }

        // dispatch messages related to state change
        OnGameStateChanged.Invoke(CurrentGameState, previousState);
    }

    public void StartSpawningCustomers()
    {
        _customerManager = Instantiate(CustomerManagerPrefab) as GameObject;
    }


    #region LOAD_UNLOAD_LEVELS
    void OnLoadOperationComplete(AsyncOperation ao)
    {
        // The method we are going to pass to the complete event listener
        // Required to take an asyncoperation as an argument
        Debug.Log("Load complete");
        if (_loadOperations.Contains(ao)) // If this completed operation exists in the list remove it
        {
            _loadOperations.Remove(ao);

            if (_loadOperations.Count == 0)
            {
                UpdateState(GameState.LEVEL_RUNNING);
            }
        }
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(LevelData.levelName));
        Debug.Log("Active Scene : " + SceneManager.GetActiveScene().name);
    }

    void OnUnloadOperationComplete(AsyncOperation ao)
    {
        Debug.Log("UnLoad complete");
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Boot"));
        Debug.Log("Active Scene : " + SceneManager.GetActiveScene().name);
    }

    public void LoadLevel(string levelName)
    {

        AsyncOperation ao = SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);

        _loadOperations.Add(ao); // Add to the list of loadoperations 
        if (ao == null)
        {
            Debug.LogError("[GameManager] Unable to load level " + levelName);
            return;
        }

        ao.completed += OnLoadOperationComplete;
    }

    public void UnloadLevel(string levelName)
    {
        // Unload scene with level name
        AsyncOperation ao = SceneManager.UnloadSceneAsync(levelName);

        if (ao == null)
        {
            Debug.LogError("[GameManager] Unable to unload level " + levelName);
            return;
        }
        ao.completed += OnUnloadOperationComplete;
    }
    #endregion

    #region PAUSING
    public void PauseGame()
    {
        UpdateState(GameState.PAUSED);
    }

    public void UnPauseGame()
    {
        UpdateState(GameState.LEVEL_RUNNING);
    }
    #endregion

    #region RESTART_AND_QUIT
    public void RestartLevel()
    {
        // Unload the level
        UnloadLevel(LevelData.levelName);
        ResetTargets();
        LoadLevel(LevelData.levelName);
    }

    public void QuitLevel()
    {
        // Revert back to the preagame state
        UpdateState(GameState.PREGAME);
        Sound_Manager.Instance.PlayMusic("ambient");
        // Unload the level
        UnloadLevel(LevelData.levelName);
    }

    public void QuitGame()
    {
        // Autosave and do some cleanup before quitting the game


        // Tears down and destroys the whole game instance, you can do some final work before this
        Application.Quit();
    }
    #endregion


    void InstantiateSystemPrefabs()
    {
        GameObject prefabInstance;

        for (int i = 0; i < SystemPrefabs.Length; ++i)
        {
            prefabInstance = Instantiate(SystemPrefabs[i]);
            _instancedSystemPrefabs.Add(prefabInstance);
        }
    }

    protected override void OnDestroy()
    { // Override to do some cleanup here. Call the base class first
        base.OnDestroy();

        // Iterate through the list of prefabs and destroy the to clean up
        for (int i = 0; i < _instancedSystemPrefabs.Count; ++i)
        {
            Destroy(_instancedSystemPrefabs[i]);
        }

        // Clear the list to remove any references to objects that were destroyed
        _instancedSystemPrefabs.Clear();
        Destroy(_customerManager);
    }

    public void ResetTargets()
    {
        CurrentRevenue = 0;
        CurrentFinIQ = 0;
        CurrentServiceRating = 0;
    }

}


public class PlayerData
{
    public static string playerName;
    public static int playerAge;
    public static int networth;
    public static int finIQ;
    public static int currentLevel;
    public static int currentChallenge;

    public static void Save()
    {
        FileManagement.SetInt("NetWorth", networth);
        FileManagement.SetInt("Level", currentLevel);
        FileManagement.SetInt("Round", currentChallenge);
    }

    public static PlayerData Load()
    {
        return null;
    }
}