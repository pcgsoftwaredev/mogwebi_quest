﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* This is a generic singleton that will be used for scripts that need to be singletons
* ie only on instance of the object is required at any point in time
*/

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    public static T Instance { get; private set; }

    public static bool isInitialized
    { // Will be handy in the future to check if an instance has been created without checking for null
        get { return Instance != null; }
    }

    protected virtual void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("[Singleton] trying to creating a second instance of a singleton class. ");
        }
        else
        {
            Instance = (T)this;
        }
    }

    // Create this singleton if somehow it gets destroyed
    protected virtual void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null; // So another instance can be created in the future
        }

    }

    
}
