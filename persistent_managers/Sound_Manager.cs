﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Hellmade.Sound;
using System;

public class Sound_Manager : Singleton<Sound_Manager>
{
    //sound controls
    [SerializeField] Slider musicVol, soundsVol;
    [SerializeField] GameObject muteMusic, unMuteMusic, muteUI, unmuteUI;

    //Music
    public AudioClip ambientMusic, L1Soundtrack, L2Soundtrack, L3Soundtrack, L4Soundtrack, L5Soundtrack, quiz, done, paused;

    //UI Sounds
    public AudioClip uiSwoosh, uiPopIn, uiPopOut, uiGenericClick, startGame, sliderClick, startRound, roundGoals;

    //Gameplay sounds
    public AudioClip sausageFrying, noStock, grabItem, serveDrink, unlockItem, trashItem, levelComplete, levelFailed, coin, correct, wrong, deepFrying, squirt, clock, roundComplete;

    // Start is called before the first frame update
    void Start()
    {

        PlayMusic("ambient");

        //add volume change listeners
        musicVol.onValueChanged.AddListener(AdjustMusicVolume);
        soundsVol.onValueChanged.AddListener(AdjustSoundsVolume);
    }

    // Update is called once per frame
    void Update()
    {
     
    }


    #region MUSIC
    public void PlayMusic(string type)
    {
        switch (type)
        {
            case "ambient":
                EazySoundManager.PlayMusic(ambientMusic, 0.5f,true,true);
                break;
            case "level1":
                EazySoundManager.PlayMusic(L1Soundtrack, 0.4f, true, false);
                break;
            case "level2":
                EazySoundManager.PlayMusic(L2Soundtrack, 0.4f, true, false);
                break;
            case "level3":
                EazySoundManager.PlayMusic(L3Soundtrack, 0.4f, true, false);
                break;
            case "level4":
                EazySoundManager.PlayMusic(L4Soundtrack, 0.4f, true, false);
                break;
            case "quiz":
                EazySoundManager.PlayMusic(quiz, 0.8f, true, false);
                break;
            case "done":
                EazySoundManager.PlayMusic(done, 0.7f, true, false);
                break;
            case "pause":
                EazySoundManager.PlayMusic(paused, 0.1f, true, false);
                break;
        }
    }


    #endregion

    #region GAMEPLAY_SOUNDS
    public int PlaySound(string type)
    {
        int soundID = 0;
        switch (type)
        {
            case "sausage_frying":
                soundID = EazySoundManager.PlaySound(sausageFrying, true);
                break;
            case "deep_frying":
                soundID = EazySoundManager.PlaySound(deepFrying, true);
                break;
            case "no_stock":
                soundID = EazySoundManager.PlaySound(noStock);
                break;
            case "grab_item":
                soundID = EazySoundManager.PlaySound(grabItem);
                break;
            case "serve_drink":
                soundID = EazySoundManager.PlaySound(serveDrink);
                break;
            case "trash_item":
                soundID = EazySoundManager.PlaySound(trashItem);
                break;
            case "level_complete":
                soundID = EazySoundManager.PlaySound(levelComplete);
                break;
            case "round_complete":
                soundID = EazySoundManager.PlaySound(roundComplete);
                break;
            case "level_failed":
                soundID = EazySoundManager.PlaySound(levelFailed);
                break;
            case "coin_sound":
                soundID = EazySoundManager.PlaySound(coin);
                break;
            case "correct":
                soundID = EazySoundManager.PlaySound(correct);
                break;
            case "wrong":
                soundID = EazySoundManager.PlaySound(wrong);
                break;
            case "squirt":
                soundID = EazySoundManager.PlaySound(squirt);
                break;
            case "time_up":
                soundID = EazySoundManager.PlaySound(clock,2f,true, transform);
                break;
        }

        return soundID;
    }
    #endregion


    public void StopSound(int ID)
    {
        Audio soundAudio = EazySoundManager.GetSoundAudio(ID);
        soundAudio.Stop();
    }

    public void PauseSound(int ID)
    {
        Audio soundAudio = EazySoundManager.GetSoundAudio(ID);
        soundAudio.Pause();
    }

    public void StopMusic()
    {
        EazySoundManager.StopAllMusic();
    }

    public void UnPauseSound(int ID)
    {
        Audio soundAudio = EazySoundManager.GetSoundAudio(ID);
        soundAudio.Resume();
    }

    public void PauseAllSounds()
    {
        EazySoundManager.PauseAllSounds();
    }

    public void StopAllSounds()
    {
        EazySoundManager.StopAllSounds();
    }

    public void ResumeAllSounds()
    {
        EazySoundManager.ResumeAllSounds();
    }


    #region UI_SOUNDS
    public void PlayUISound(string type)
    {
        switch (type)
        {
            case "popin":
                EazySoundManager.PlayUISound(uiPopIn);
                break;
            case "popout":
                EazySoundManager.PlayUISound(uiPopOut);
                break;
            case "generic":
                EazySoundManager.PlayUISound(uiGenericClick);
                break;
            case "startGame":
                EazySoundManager.PlayUISound(startGame);
                break;
            case "startRound":
                EazySoundManager.PlayUISound(startRound);
                break;
            case "swoosh":
                EazySoundManager.PlayUISound(uiSwoosh);
                break;
            case "roundGoals":
                EazySoundManager.PlayUISound(roundGoals);
                break;
            case "click":
                EazySoundManager.PlayUISound(sliderClick, 0.4f);
                break;
        }
    }
    #endregion



    #region SOUNDCONTROLS

    void AdjustMusicVolume(float volume)
    {
        EazySoundManager.GlobalMusicVolume = volume;
        EazySoundManager.PlayUISound(sliderClick,volume);
    }

    void AdjustSoundsVolume(float volume)
    {
        EazySoundManager.GlobalSoundsVolume = volume;
        EazySoundManager.GlobalUISoundsVolume = volume;
        EazySoundManager.PlayUISound(sliderClick, volume);
    }

    public void ReduceVolume()
    {
        EazySoundManager.GlobalMusicVolume = 0.3f;
    }

    public void ToggleMuteMusic()
    {
        float EPSILON = 0.01f;
        if (Math.Abs(EazySoundManager.GlobalMusicVolume) < EPSILON)
        {
            EazySoundManager.GlobalMusicVolume = musicVol.value;
            unMuteMusic.SetActive(false);
            muteMusic.SetActive(true);
            musicVol.interactable = true;
        }
        else
        {
            EazySoundManager.GlobalMusicVolume = 0;
            unMuteMusic.SetActive(true);
            muteMusic.SetActive(false);
            musicVol.interactable = false;
        }
    }

    public void ToggleMuteUISfx()
    {
        float EPSILON = 0.01f;
        if (Math.Abs(EazySoundManager.GlobalSoundsVolume) < EPSILON)
        {
            EazySoundManager.GlobalSoundsVolume = soundsVol.value;
            EazySoundManager.GlobalUISoundsVolume = soundsVol.value;
            unmuteUI.SetActive(false);
            muteUI.SetActive(true);
            soundsVol.interactable = true;
        }
        else
        {
            EazySoundManager.GlobalSoundsVolume = 0;
            EazySoundManager.GlobalUISoundsVolume = 0;
            unmuteUI.SetActive(true);
            muteUI.SetActive(false);
            soundsVol.interactable = false;
        }
    }
    #endregion

}
