﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Manager : Singleton<UI_Manager>
{

    //main canvas for all UI screens
    public GameObject mainCanvas;

    //ui gameobjects
    public GameObject launchMenu;
    public GameObject optionsMenu;
    public GameObject levelSelectionMenu;
    public GameObject advisor;
    public GameObject hud, overlay, feedback;
    public GameObject financialServicesStore;
    public GameObject pauseMenu;
    public GameObject characterSelectionMenu;

    public Button advisorButton, goalsButton;

    private GameObject instantiatedGameObject;


    // Start is called before the first frame update
    void Start()
    {
        //persist UI manager across scenes
        DontDestroyOnLoad(gameObject);
        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);
         DisplayLaunchMenu();
    }

    void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState previousState)
    {
        // Set the level menu active and inactive depending on whether we are returning from the running state to pregame or not
        if ((currentState == GameManager.GameState.PREGAME))
        {
            DisplayLevelSelectionMenu();
        }

        if (currentState == GameManager.GameState.LEVEL_RUNNING)
        {
            DisplayGameplayHUD();
        }
    }


    public void DisplayLaunchMenu()
    {
        ClearUI();
        launchMenu.SetActive(true);
    }

    public void DisplayCharacterSelectionMenu()
    {
        ClearUI();
        Sound_Manager.Instance.PlayUISound("generic");
        characterSelectionMenu.SetActive(true);
    }

    /// <summary>
    /// Level selection menu
    /// </summary>
    public void DisplayLevelSelectionMenu()
    {
        Sound_Manager.Instance.PlayUISound("startGame");
        ClearUI();
        levelSelectionMenu.SetActive(true);
    }

    public void CloseLevelSelectionMenu()
    {
        ClearUI();
        levelSelectionMenu.SetActive(false);
    }

    /// <summary>
    /// Financial services advisor popup
    /// </summary>
    public void DisplayAdvisor()
    {
        Sound_Manager.Instance.PlayUISound("popin");
        GameManager.Instance.PauseGame();
        //ClearUI();
        //advisor.SetActive(true);
    }

    public void CloseAdvisor()
    {
        Sound_Manager.Instance.PlayUISound("popout");
        advisor.SetActive(false);
        DisplayGameplayHUD();
        GameManager.Instance.UnPauseGame();
    }

    /// <summary>
    /// show/hide gameplay HUD
    /// </summary>
    public void DisplayGameplayHUD()
    {
        ClearUI();
        hud.SetActive(true);
    }

    public void HideGamePlayHUD()
    {
        hud.SetActive(false);
    }

    /// <summary>
    /// Financial services store popup
    /// </summary>
    public void DisplayFinancialServicesStore()
    {
        Sound_Manager.Instance.PlayUISound("popin");
        GameManager.Instance.PauseGame();
        ClearUI();
        financialServicesStore.SetActive(true);
    }

    public void CloseFinancialServicesStore()
    {
        Sound_Manager.Instance.PlayUISound("popout");
        financialServicesStore.SetActive(false);
        DisplayGameplayHUD();
        GameManager.Instance.UnPauseGame();
    }

    public void ToggleDarkOverlay()
    {
        overlay.SetActive(!overlay.activeSelf);
    }

    public void CloseOptionsMenu()
    {
        Sound_Manager.Instance.PlayUISound("popout");
        optionsMenu.SetActive(false);
    }

    public void ClosePauseMenu()
    {
        Sound_Manager.Instance.PlayUISound("popout");
        pauseMenu.SetActive(false);
        GameManager.Instance.UnPauseGame();
    }

    //public void DisplayFinanceManagementInterface()
    //{
    //    Sound_Manager.Instance.PlayUISound("popin");
    //    GameManager.Instance.TogglePause();
    //    financeManagement.SetActive(true);
    //}

    //Clear the main canvas
    public void ClearUI()
    {
        foreach (Transform child in mainCanvas.transform)
        {
            child.gameObject.SetActive(false);
        }
    }


    //HANDLE UI BUTTONS

    public void HandlePause()
    {
        Sound_Manager.Instance.PlayUISound("popin");
        GameManager.Instance.PauseGame();
        pauseMenu.SetActive(true);
    }

    public Button AdvisorButton
    {
        get { return advisorButton; }
    }

    public void HandleOptionsButton(bool gameIsStarted)
    {
        Sound_Manager.Instance.PlayUISound("popin");
        if (gameIsStarted)
            GameManager.Instance.PauseGame();
        optionsMenu.SetActive(true);
    }

    public void GetFeedBack()
    {
        feedback.SetActive(true);
        Sound_Manager.Instance.StopAllSounds();
    }








    #region COROUTINES
    IEnumerator ShowRoundGoals()
    {
        yield return null;
    }

    IEnumerator RoundCompleteRoutine()
    {
        Sound_Manager.Instance.StopMusic();
        Sound_Manager.Instance.PauseAllSounds();
        
        yield return null;
    }

    IEnumerator LevelCompleteRoutine()
    {
        yield return null;
    }

    IEnumerator GameOverRoutine()
    {
        yield return null;
    }
    #endregion

}
