﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BunsContainer : Singleton<BunsContainer>
{
    private Animator anim;
    private BoxCollider2D tapArea;
    public GameObject HotDogBun;
    private GameObject bunsWidget;
    public static int BunCount { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        
        switch (GameManager.LevelData.levelNumber)
        {
            case 1:
                bunsWidget = Level_1.Instance.stockCntWidgets["buns"];
                break;
        }

        BunCount = int.Parse(bunsWidget.GetComponentInChildren<TextMeshProUGUI>().text) ;

        tapArea = GetComponent<BoxCollider2D>();
        anim = GetComponent<Animator>();

        GameManager.Instance.OnGameStateChanged.AddListener(DisableCollider);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void DisableCollider(GameManager.GameState currentState, GameManager.GameState prevState)
    {
        if (currentState == GameManager.GameState.PAUSED)
            tapArea.enabled = false;

        if (currentState == GameManager.GameState.LEVEL_RUNNING)
            tapArea.enabled = true;
    }

    private void OnMouseDown()
    {
        if (BunCount > 0 && GameManager.LevelPrepared)
        {
            Tray.Instance.PlaceItem(HotDogBun);
            // Play the animation for feedback
            anim.Play("hotDogsContainerAnim", 0);
            Sound_Manager.Instance.PlaySound("grab_item");
            bunsWidget.GetComponentInChildren<TextMeshProUGUI>().text = BunCount.ToString();
        }
        else
        {
             Sound_Manager.Instance.PlaySound("no_stock");
        }

        
    }
}
