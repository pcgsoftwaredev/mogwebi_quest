﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CadacGriller : Singleton<CadacGriller>
{
    [SerializeField] private GameObject steamBubbles;
    [SerializeField] private GameObject spot1, spot2;

    

    private bool spot1Free, spot2Free;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        spot1Free = spot1.transform.childCount == 0;
        spot2Free = spot2.transform.childCount == 0;
        if (spot1Free  && spot2Free )
        {
            steamBubbles.SetActive(false);
        }

        if (!spot1Free || !spot2Free)
        {
            steamBubbles.SetActive(true);
        }
    }

    public bool Grill(GameObject item)
    {
        if (spot1Free)
        {
            Instantiate(item, spot1.transform.position, Quaternion.identity, spot1.transform);
            steamBubbles.SetActive(true);
            HotDogsContainer.HotDogCount--;
            return true;
        }

        if (spot2Free)
        {
            Instantiate(item, spot2.transform.position, Quaternion.identity,spot2.transform);
            steamBubbles.SetActive(true);
            HotDogsContainer.HotDogCount--;
            return true;
        }
        //handle no spots free, if we reached here
        return false;	
    }
}
