﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoolerBox : Singleton<CoolerBox>
{
    private Animator anim;
    private GameObject coolerWidget;

    public static int SoftDrinkCount { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        switch (GameManager.LevelData.levelNumber)
        {
            case 1:
                coolerWidget = Level_1.Instance.stockCntWidgets["drink"];
                break;
        }
        SoftDrinkCount = int.Parse(coolerWidget.GetComponentInChildren<TextMeshProUGUI>().text);
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        if(DrinksArea.isInitialized)
            DrinksArea.Instance.PlaceSoftDrinkOrder();

        Sound_Manager.Instance.PlaySound("grab_item");
        coolerWidget.GetComponentInChildren<TextMeshProUGUI>().text = SoftDrinkCount.ToString();
        // Play the animation for feedback
        anim.Play("hotDogsContainerAnim", 0);
    }

}
