﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleDeepFry : Singleton<DoubleDeepFry>
{
    [Header("Double Fry Positions")]
    [Space(10)]
    [SerializeField]private Transform friesSpot;
    [SerializeField]private Transform fatCakesSpot;

    public static bool FriesCooking{ get; set; }
    public static bool DoughCooking { get; set; }

    public Vector2 FriesSpot {
        get { return friesSpot.position; }
    }

    public Vector2 FatCakesSpot
    {
        get { return fatCakesSpot.position; }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (!FriesCooking && !DoughCooking)
            StopFryingSounds();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void StopFryingSounds()
    {
        // Stop grilling sounds if no hotdog is on the grill, or fries or fat cakes
        if (LargeGrill.isInitialized)
        {
            if (!LargeGrill.Instance.IsGrillGrilling())
                SoundManager.Instance.StopOilFryingSound();
        }
      
    }
}
