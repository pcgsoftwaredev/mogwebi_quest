﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrinksArea : Singleton<DrinksArea>
{

    [SerializeField] private GameObject softDrinkPref;
    public static GameObject[] drinkOrdersArr = new GameObject[2];

    // Positions on the drinks serving area
    [SerializeField]private GameObject drinksAreaLocation1;
    [SerializeField]private GameObject drinksAreaLocation2;

    private float ellapsedTime = 0;
    // Disables placing hotdog on the grill until the animation is complete and also
    // after the player is given an opportunity to upgrade the grill
    private float delayCookingTime = 4.0f;
    private float delaySettingPositions = 7.0f;

    // Start is called before the first frame update
    void Start()
    {
        // Tic Toc
        ellapsedTime += Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlaceSoftDrinkOrder()
    {
        if (CoolerBox.isInitialized)
        {
            if (CoolerBox.SoftDrinkCount > 0)
            {
                if (drinkOrdersArr[0] == null)
                { // First slot is empty, place hot dog in it
                    GameObject drink = Instantiate(softDrinkPref, drinksAreaLocation1.transform.position, Quaternion.identity) as GameObject;
                    drink.GetComponentInChildren<SpriteRenderer>().sortingOrder = 4;
                    drinkOrdersArr[0] = drink;

                    CoolerBox.SoftDrinkCount--;
                }
                else if (drinkOrdersArr[1] == null)
                {
                    GameObject drink = Instantiate(softDrinkPref, drinksAreaLocation2.transform.position, Quaternion.identity) as GameObject;
                    drink.GetComponentInChildren<SpriteRenderer>().sortingOrder = 5;
                    drinkOrdersArr[1] = drink;
                    CoolerBox.SoftDrinkCount--;
                }
            }
        }
    }
}
