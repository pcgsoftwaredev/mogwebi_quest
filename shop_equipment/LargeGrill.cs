﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LargeGrill : Singleton<LargeGrill>
{
    [SerializeField] private GameObject hotDogPref;
    [SerializeField] private GameObject smokeEffect;
    [SerializeField] private GameObject glitterEffect;

    // Four places available to put hotdogs
    public static GameObject[] hotdogsArr = new GameObject[4];

    public static Vector2 firstLargeGrillSlotPos = new Vector2(0, 0);
    public static Vector2 secondLargeGrillSlotPos = new Vector2(0, 0);
    public static Vector2 thirdLargeGrillSlotPos = new Vector2(0, 0);
    public static Vector2 fourthLargeGrillSlotPos = new Vector2(0, 0);

    private float ellapsedTime = 0;
    // Disables placing hotdog on the grill until the animation is complete and also
    // after the player is given an opportunity to upgrade the grill
    private float delayCookingTime = 4.0f;
    private float delaySettingPositions = 6.0f;

    public bool thereIsACookingHotdog;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(PlayGlitterEffectCo());
    }

    // Update is called once per frame
    void Update()
    {
        // Tic Toc
        ellapsedTime += Time.deltaTime;

        if (ellapsedTime < delaySettingPositions)
        {
            // LargeGrillSlots
            firstLargeGrillSlotPos = new Vector2(transform.position.x - 0.8f, transform.position.y + 1.0f);
            secondLargeGrillSlotPos = new Vector2(transform.position.x + 1.1f, transform.position.y + 1.0f);
            thirdLargeGrillSlotPos = new Vector2(transform.position.x - 1.0f, transform.position.y + 1.5f);
            fourthLargeGrillSlotPos = new Vector2(transform.position.x + 1.0f, transform.position.y + 1.5f);

        }

        // Continually check grill for burning hotdog
        if (GrillHasBurntHotDog())
        {
            if (!smokeEffect.activeSelf)
                smokeEffect.SetActive(true);
        }
        else
        {
            if (smokeEffect.activeSelf)
                smokeEffect.SetActive(false);
        }
    }

    public void PlaceHotDogOnLargeGrill()
    {
        //if (hotdogsArr[0] == null) // If there is nothing in the array spot
        //{ // First slot is empty
        //    GameObject hotdog = Instantiate(hotDogPref, LargeGrill.firstLargeGrillSlotPos, Quaternion.identity) as GameObject;
        //    hotdogsArr[0] = hotdog;

        //    // Set the current slot of the specific hotdog
        //    hotdog.GetComponent<Sausage>().currentGrillSlot = 0;

        //    // Update hotdog count
        //    if (SausagesBox.isInitialized)
        //        SausagesBox.HotDogCount--;
        //}
        //else if (hotdogsArr[1] == null) // If there is nothing in the array spot
        //{ // First slot is empty
        //    GameObject hotdog = Instantiate(hotDogPref, LargeGrill.secondLargeGrillSlotPos, Quaternion.identity) as GameObject;
        //    hotdogsArr[1] = hotdog;

        //    // Set the current slot of the specific hotdog
        //    hotdog.GetComponent<Sausage>().currentGrillSlot = 1;

        //    // Update hotdog count
        //    if (SausagesBox.isInitialized)
        //        SausagesBox.HotDogCount--;
        //}
        //else if (hotdogsArr[2] == null) // If there is nothing in the array spot
        //{ // First slot is empty
        //    GameObject hotdog = Instantiate(hotDogPref, LargeGrill.thirdLargeGrillSlotPos, Quaternion.identity) as GameObject;
        //    hotdogsArr[2] = hotdog;

        //    // Set the current slot of the specific hotdog
        //    hotdog.GetComponent<Sausage>().currentGrillSlot = 2;

        //    // Update hotdog counts
        //    if (SausagesBox.isInitialized)
        //        SausagesBox.HotDogCount--;
        //}
        //else if (hotdogsArr[3] == null) // If there is nothing in the array spot
        //{ // First slot is empty
        //    GameObject hotdog = Instantiate(hotDogPref, LargeGrill.fourthLargeGrillSlotPos, Quaternion.identity) as GameObject;
        //    hotdogsArr[3] = hotdog;

        //    // Set the current slot of the specific hotdog
        //    hotdog.GetComponent<Sausage>().currentGrillSlot = 3;

        //    // Update hotdog count
        //    if (SausagesBox.isInitialized)
        //        SausagesBox.HotDogCount--;
        //}
    }

    public void ClearGrillLocation(int grillLocation)
    {
        hotdogsArr[grillLocation] = null;
    }


    public bool GrillHasBurntHotDog()
    {// check of the grill has a burning hotdog
        bool thereIsAburntHotDog = false;

        foreach (GameObject hotdog in hotdogsArr)
        {
            if (hotdog != null)
            { // if a hotdog exists
                thereIsACookingHotdog = true;
                if (hotdog.GetComponent<Sausage>().state == Sausage.SausageState.burnt)
                { // If you find a burnt hotdog start the smoke, else remove the smoke
                    thereIsAburntHotDog = true;
                    return thereIsAburntHotDog;
                }
            }
        }
        return thereIsAburntHotDog;
    }

    public bool IsGrillGrilling()
    { // check for cooking hotdog, return true if atleast 1 hotdog is cooking on the grill
        bool isGrilling = false;

        foreach (GameObject hotdog in hotdogsArr)
        {
            if (hotdog != null)
            {
                isGrilling = true;
                break;
            }
        }
        return isGrilling;
    }

    private IEnumerator PlayGlitterEffectCo()
    {
        glitterEffect.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        glitterEffect.SetActive(false);
    }
}
