﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PotatoesContainer : MonoBehaviour
{
    [SerializeField] private TMP_Text hotDogsCountTxt;
    [SerializeField] private GameObject cookingFries;
    public static int PotatoesCount { get; set; }

    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        PotatoesCount = 20;

        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        hotDogsCountTxt.text = PotatoesCount + "";
    }

    private void OnMouseDown()
    {
        // Stark cooking mabanse on the second cooking area of the double deef fryer
        if (DoubleDeepFry.isInitialized)
        {
            if (!DoubleDeepFry.FriesCooking)
            {
                Instantiate(cookingFries, DoubleDeepFry.Instance.FriesSpot, Quaternion.identity);

                PotatoesCount -= 2;

                DoubleDeepFry.FriesCooking = true;
            }
        }
        else if (SingleDeepFryer.isInitialized)
        {
            if (!SingleDeepFryer.FriesCooking)
            {

                Instantiate(cookingFries, new Vector2(
                    SingleDeepFryer.Instance.gameObject.transform.position.x,
                    SingleDeepFryer.Instance.gameObject.transform.position.y + 0.3f), Quaternion.identity);

                PotatoesCount -= 2;

                SingleDeepFryer.FriesCooking = true;
            }
        }

        // Play the animation for feedback
        anim.Play("PotatoesAnim", 0);
    }
}
