﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServingContainer : Singleton<ServingContainer>
{
    [SerializeField]private GameObject friesOrderObj;
    [SerializeField]private GameObject fatCakeOrderObj;

    public static GameObject[] friesOrdersArr = new GameObject[2];
    public static GameObject[] fatCakesOrdersArr = new GameObject[2];

    private float ellapsedTime = 0;
    // Disables placing hotdog on the grill until the animation is complete and also
    // after the player is given an opportunity to upgrade the grill
    private float delayCookingTime = 4.0f;
    private float delaySettingPositions = 6.0f;

    [Header("Tray Positions")]
    [Space(10)]
    [SerializeField] private Transform firstFriesSpot;
    [SerializeField] private Transform secondFriesSpot;
    [SerializeField] private Transform firstFatCakesSpot;
    [SerializeField] private Transform secondFatCakesSpot;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaceFriesOrder()
    {
        // Put a bun in the serving area, in one of the four slots

        if (friesOrdersArr[0] == null)
        { // First slot is empty, place hot dog in it
            GameObject fries = Instantiate(friesOrderObj, firstFriesSpot.position, Quaternion.identity) as GameObject;
            friesOrdersArr[0] = fries;
            fries.GetComponentInChildren<SpriteRenderer>().sortingOrder = 4;
        }
        else if (friesOrdersArr[1] == null)
        {
            GameObject fries = Instantiate(friesOrderObj, secondFriesSpot.position, Quaternion.identity) as GameObject;
            friesOrdersArr[1] = fries;
            fries.GetComponentInChildren<SpriteRenderer>().sortingOrder = 3;
        }
    }

    public void PlaceFatCakesOrder()
    {
        // Put a bun in the serving area, in one of the four slots

        if (fatCakesOrdersArr[0] == null)
        { // First slot is empty, place hot dog in it
            GameObject fatCake = Instantiate(fatCakeOrderObj, firstFatCakesSpot.position, Quaternion.identity) as GameObject;
            fatCakesOrdersArr[0] = fatCake;
            fatCake.GetComponentInChildren<SpriteRenderer>().sortingOrder = 2;
            //fatCakesCount--;
        }
        else if (fatCakesOrdersArr[1] == null)
        {
            GameObject fatCake = Instantiate(fatCakeOrderObj, secondFatCakesSpot.position, Quaternion.identity) as GameObject;
            fatCakesOrdersArr[1] = fatCake;
            fatCake.GetComponentInChildren<SpriteRenderer>().sortingOrder = 1;
            //fatCakesCount--;
        }
    }
}
