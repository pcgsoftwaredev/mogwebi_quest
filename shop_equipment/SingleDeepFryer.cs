﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleDeepFryer : Singleton<SingleDeepFryer>
{
    private Animator anim;
    public static bool FriesCooking { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnMouseDown()
    {

    }

    void StopFryingSounds()
    {
        // Stop grilling sounds if no hotdog is on the grill, or fries or fat cakes
        if (LargeGrill.isInitialized)
        {
            if (!LargeGrill.Instance.IsGrillGrilling())
                SoundManager.Instance.StopOilFryingSound();
        }
        
    }
}
