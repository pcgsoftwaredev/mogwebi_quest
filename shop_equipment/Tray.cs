﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tray : Singleton<Tray>
{

    [SerializeField] private GameObject spot1, spot2, spot3, spot4;
    private TraySpot[] traySpots;
    private GameObject[] spotMarkers;
    private TraySpot s1, s2, s3, s4;

    public Queue<GameObject> CurrentlySelectedTrayItem;

    //struct to track status of a tray slot
    public struct TraySpot
    {
        public bool _locked, _occupied;
        public TraySpot(bool locked, bool occupied)
        {
            _locked = locked;
            _occupied = occupied;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        bool[] TraySpotStatus = null;
        if (GameManager.CurrentRound == 0)
            TraySpotStatus = TraySpotLocked(false, true, true, true);
        if (GameManager.CurrentRound == 1)
            TraySpotStatus = TraySpotLocked(false, false, true, true);
        if (GameManager.CurrentRound == 2)
            TraySpotStatus = TraySpotLocked(false, false, false, false);

        s1 = new TraySpot(TraySpotStatus[0], false); 
        s2 = new TraySpot(TraySpotStatus[1], false);
        s3 = new TraySpot(TraySpotStatus[2], false);
        s4 = new TraySpot(TraySpotStatus[3], false);

        traySpots = new TraySpot[] { s1, s2, s3, s4};
        spotMarkers = new GameObject[] { spot1, spot2, spot3, spot4};

        //remove padlock from unlocked slots
        for (int i=0; i<traySpots.Length; i++)
        {
            if (!traySpots[i]._locked)
            {
                gameObject.transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
            }
        }

        CurrentlySelectedTrayItem = new Queue<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //returns lock status of tray spots
    public bool[] TraySpotLocked(bool a, bool b, bool c, bool d)
    {
        //should get values from playerprefs, for now we hardcode them
        return new bool[] { a, b, c, d};
    }

    public void SelectTrayItem(GameObject item)
    {
        if (CurrentlySelectedTrayItem.Count != 0)
        {
           // print("DE-Selected: " + CurrentlySelectedTrayItem.Peek().GetInstanceID());
            CurrentlySelectedTrayItem.Dequeue();
        }

        CurrentlySelectedTrayItem.Enqueue(item);
        //print("Selected: "+ CurrentlySelectedTrayItem.Peek().GetInstanceID());
    }

    //place an item on the tray
    public void PlaceItem(GameObject item)
    {
        GameObject spot = ChooseSpot();
        
        if (spot == null)
        {
            Sound_Manager.Instance.PlaySound("stock_empty");
            return;
        }
       
        Instantiate(item, spot.transform.position, Quaternion.identity, spot.transform);
        BunsContainer.BunCount--;
        SelectTrayItem(item);
    }

    public void FreeSpot()
    {
        for(int i=0; i < traySpots.Length; i++)
        {
            if(spotMarkers[i].transform.childCount == 1)
            {
                traySpots[i]._occupied = false;
            }
        }
    }

    //select an unlocked and unoccupied spot on the tray to place an item
    private GameObject ChooseSpot()
    {
        GameObject spot = null;
        for(int i=0; i<traySpots.Length; i++)
        {
            if (!traySpots[i]._locked && !traySpots[i]._occupied)
            {
                spot = spotMarkers[i];
                traySpots[i]._occupied = true;
                break;
            }
        }
        return spot;
    }
}
